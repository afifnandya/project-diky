<div class="col-xs-12">
<div class="box">
<div class="box-header">
<h3 class="box-title"><i class="glyphicon glyphicon-th"></i> Barcode</h3>
</div>
<div class="box-body">

<div class="nav-tabs-custom">
<ul class="nav nav-tabs">
<li class="active"><a href="#tab_1" data-toggle="tab">Cetak Produk</a></li>
<li style="display:none"><a href="#tab_2" data-toggle="tab">Cetak Banyak Produk</a></li>
</ul>
<div class="tab-content">
<div class="tab-pane active" id="tab_1">

<table class="table table-bordered">
<tr><td style="width:300px">
<div class="input-group">
<input class="form-control" type="text" id="kodeBarang" readonly>
<span class="input-group-btn">
<button class="btn btn-default" type="button" id="showBarang"><i class="fa fa-search" aria-hidden="true"></i> Cari</button></span>
</div>
<input style="margin-top:5px" class="form-control" type="text" id="namaBarang">
<input style="margin-top:5px" class="form-control" type="text" id="hargaJual">
<div class="input-group" style="margin-top:5px" >
<span class="input-group-btn"><button class="btn btn-default" type="button">Jumlah</button></span>
<input class="form-control" type="number" id="qty" value="1" style="width:100px;text-align:center">
</div>


<a class="btn btn-warning" href="#" id="showBarcodes" style="margin-top:5px" ><i class="fa fa-check" aria-hidden="true"></i> Tampilkan Barcode</a>


</td>
<td>
<div id="getBarcode"></div>

</td></tr>
</table>
</div>
<div class="tab-pane" id="tab_2">
<div style="overflow-y: auto; height:430px; ">

<div id="printArea">
<?php
/*
$items=doTableArray("daftar_barang",array("kode_barang","nama_barang","harga_jual"));
$i=1;
foreach($items as $row){

?>
<div class="col-md-3 ">
<?php 
echo '
<div style="height:100px;width:220px;float:left;margin:5px;border:1px solid #ccc;padding:5px">
<center>
<span style="font-size:11px">'.$row[1].'</span><br><img  src="barcode.php?codetype=Code39&size=28&print=true&text='.$row[0].'"></center>
</div>';
?>
</div>
<?php
$i++; 	  
}  
*/
?>
</div>
</div>
<br>
<a class="btn btn-primary" onclick="jQuery('#printArea').print()" href="#" id="printPenjualan"><i class="fa fa-print" aria-hidden="true"></i> Cetak Barcode</a>
</div>
</div>
<!-- /.tab-content -->
</div>
</div>
</div>
</div>






