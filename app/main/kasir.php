<?php

if(trim($xdata)==getPengaturan('aktivasi') || 
(HDDLabel()!='' AND numHash(hexdec(HDDLabel()))==getPengaturan('aktivasi')) ||
(trim(getPengaturan('serial'))!='' AND numHash(getPengaturan('serial'))==trim(getPengaturan('aktivasi')))
){
	
}
?>
<div class="col-md-4">
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <form id="addCartForm" action="" method="post">
                <input type="hidden" name="addCart" value="kasir_penjualan">
                <input type="hidden" name="user_id" value="<?php echo userID($_SESSION['user']);?>"
                    placeholder="user_id">
                <input type="hidden" name="faktur" value="<?php echo getFakturID('','PJ');?>" placeholder="faktur">
                <div class="input-group">
                    <span class="input-group-btn"><button class="btn " type="button">Kode Barang </button></span>
                    <input class="form-control" type="text" id="kodeBarang" autofocus title="kode barang" name="kode_barang">
                    <input class="form-control" type="text" autofocus title="kode barang" id="kodeBarangUtama" style="display:none">
                    <input class="form-control" type="text" autofocus title="kode barang" id="kodeBarangBonus" style="display:none">
                    <input class="form-control" type="text" autofocus title="kode barang" id="namaBarangUtama" style="display:none">
                    <input class="form-control" type="text" autofocus title="kode barang" id="namaBarangBonus" style="display:none">
                    <span class="input-group-btn">
                        <button class="btn " type="button" id="showBarang" title="cari barang atau tekan F1"><i
                                class="fa  fa-search" aria-hidden="true"></i> [F1]</button>
                    </span>
                </div>
                <input style="display:none;margin-top:5px" class="form-control" type="text" id="idBarang">
                <input style="display:block;margin-top:5px" class="form-control" type="text" id="namaBarang" readonly
                    placeholder="Nama Barang">
                <input style="display:block;margin-top:5px" class="form-control" type="text" id="hargaJual" readonly
                    placeholder="Harga">
                <input style="display:block;margin-top:5px;display:none" class="form-control" type="text"
                    id="stokBarang" readonly placeholder="Stok">
                <div class="input-group" style="margin-top:5px">
                    <span class="input-group-btn"><button class="btn " type="button" style="width:111px">Jumlah
                        </button></span>
                    <input style="text-align:center" id="qty" name="qty" type="number" value="1" class="form-control "
                        title="jumlah barang">
                    <span class="input-group-btn">
                        <input type="submit" value="Tambah" class="btn btn-primary" style="display:none" />
                        <span class="input-group-btn"><button class="btn btn-warning" type="button" id="addCart"><i
                                    class="fa  fa-check-square-o" aria-hidden="true"></i> Tambah</button></span>
                    </span>
                </div>
            </form>
            <div>
                <a href="#" class='btn btn-primary btn-lg' id='bayarKasir' style="margin-top:10px;width:100%"><i
                        class='fa fa-print'></i> Selesai </a>
            </div>
        </div>
    </div>
</div>
<div class="col-md-8">
    <div class="box">
        <div class="box-body">
            <span id="totalSum"></span>
            <span id="totalSumBayar" style="display:none"></span>
        </div>
    </div>
    <!-- <div class="box" id="headB" style="display:none">
        <div class="box-body" id="headBundle">
        </div>
    </div> -->
    <div class="box">
        <!-- /.box-header -->
        <div id="hiddenBundle"></div>
        <div class="box-body">
            <div class="page-header">
                <h1 id="header"></h1>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover " id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width:20px">ID</th>
                            <th>Kode</th>
                            <th>Nama Barang</th>
                            <th>Harga </th>
                            <th style="width:80px">Qty</th>
                            <th>Sub Total</th>
                            <th style="width:50px">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>