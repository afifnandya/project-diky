<?php 
$username=$_SESSION['user'];
$userlevel=userLevel($username);
?>
<style>
.bgColor label{
font-weight: bold;
color: #A0A0A0;
}
#targetLayer{
float:left;
width:150px;
height:150px;
text-align:center;
line-height:150px;
font-weight: bold;
color: #C0C0C0;
background-color: #F0E8E0;
border-radius: 4px;
}
#uploadFormLayer{
padding-top:5px
}
.btnSubmit {
	background-color: #696969;
    padding: 5px 30px;
    border: #696969 1px solid;
    border-radius: 4px;
    color: #FFFFFF;
    margin-top: 10px;
}

.image-preview {	
width:150px;
height:150px;
border-radius: 4px;
}
table.dataTable.select tbody tr,
table.dataTable thead th:first-child {
  cursor: pointer;
}
</style>
<script src="<?php echo $CORE_URL;?>/assets/plugins/air-datepicker/js/datepicker.min.js"></script>
<script src="<?php echo $CORE_URL;?>/assets/plugins/air-datepicker/js/i18n/datepicker.id.js"></script>
<script>
var ajaxData="data.php?tableJasa=jasa";
shortcut.add("f1",function() {
$('#EditPost').modal('show');
});
//$.fn.dataTable.ext.errMode = 'throw';

$(document).ready(function() {
		$("#uploadForm").on('submit',(function(e) {
		var fileExcel=$("#fileExcel").val();

		if(fileExcel==''){
			swal("","File masih kosong!").then((value) => {
				$('#fileExcel').focus();
			});
			return false;
		}
		e.preventDefault();
		$.ajax({
        	url: "import.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
			$("#data").html(data);
			table.ajax.url( ajaxData ).load();

		    },
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
//$('#selectSatuan').hide();
//$('#selectKategori').hide();
//$('[data-toggle="tooltip"]').tooltip(); 
$('#loadUploader').load('data.php?imgUpload=img&imgID=');
    var table = $('#dataTable').DataTable( {
    "language": {
      "emptyTable": "No data available in table"
    },
		  "rowCallback": function(row, data, dataIndex){

      },
	  
		scrollY: '46vh',
		scrollX: true,
		scrollCollapse: true,
		select: true,		
		responsive: true,
		 "ordering": true,
//		bSort: false,
/* 		dom: 'Bfrtip',		

       buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
		*/
		"pageLength": 20,
		"paginate":true,
		"filter":true,
		"info":true,
		"length":false,
        "ajax": ajaxData ,
		"bLengthChange": false,
		"order": [[ 4, "desc" ]],
        "columnDefs": [ {
            "targets": 0,
            "data": null,
            "defaultContent": "<div style='width:60px'><button  <?php displayAkses('jasa_edit',$userlevel);?> class='btn btn-default btn-xs' id='edit'><i class='fa fa-pencil-square-o'></i></button> <button <?php displayAkses('jasa_hapus',$userlevel);?> class='btn btn-default btn-xs' id='delete'><i class='fa fa-trash-o'></i></button></div>"
        }],


    } );
	
	$('#dataTable tbody').on( 'click', '#delete', function () {
        var data = table.row( $(this).parents('tr') ).data();


	swal({
	  title: 'Hapus',
	  html: "Anda ingin menghapus data ini? <br> <strong>Nama Jasa </strong>: "+data[ 2 ],
	  type: 'warning',
	  
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Ya, Hapus!'
	}).then((result) => {
	  if (result.value) {
		swal({  
		title: 'Hapus',
		text: 'Data berhasil dihapus',
		type: 'success',
		timer: 2000
	}
	);
			$.get("data.php?deleteJasa="+data[ 0 ],
			function(data){
			table.ajax.url( ajaxData ).load();
			 $(this).parents('tr').fadeOut(300);

			}
			);
  }
})
	
		 //table.ajax.url( 'data.txt' ).load();
    } );
	

		
	$('#dataTable tbody').on( 'click', '#edit', function () {
	var data = table.row( $(this).parents('tr') ).data();
		$('#EditPost').modal('show');
		//$('#EditPostLabel').html(data[ 0 ]);
		$('#id_jasa').val(data[ 0 ]);
		$('#kode_jasa').val(data[ 1 ]);
		$('#imgID').val(data[ 1 ]);
		$('#nama_jasa').val(data[ 2 ]);
		$('#harga_jasa').val(data[ 3 ].replace(",", ""));
		$('#reset').hide();
		$('#SaveEdit').show();
		$('#SaveInput').hide();
		$('#loadUploader').load('data.php?imgUpload=img&imgID='+data[ 1 ]);

    } );
	
	$( "#pilihKategori" ).click(function () {
			//$('#selectKategori').toggle();
			$('#EditKategori').modal("show");
    } );
	$( "#pilihSatuan" ).click(function () {
			//$('#selectSatuan').toggle();
		$('#EditSatuan').modal("show");

    } );
	
$( "#category" ).click(function () {
var category = $(this).val();

} );

$( "#new" ).click(function () {
	//$('#selectKategori').hide();
	//$('#selectSatuan').hide();
	$('#SaveEdit').hide();
	$('#reset').show();
	$('#SaveInput').show();
	$('#EditPost').modal('show');
	$('#kode_jasa').val('');
	$('#nama_jasa').val('');
	$('#harga_jasa').val('');
	// $( "#selectSatuan" ).load( "data.php?satuanBarang=satuan" );
	// $( "#selectKategori" ).load( "data.php?kategoriBarang=kategori" );
	$('#loadUploader').load('data.php?imgUpload=img&imgID=0');

} );

$( "#reset" ).click(function () {

	$('#kode_jasa').val('');
	$('#nama_jasa').val('');
	$('#harga_jasa').val('');
	$('#loadUploader').load('data.php?imgUpload=img&imgID=0');

} );

$( "#SaveInput" ).click(function () {
	var kode_jasa = $('#kode_jasa').val();
	var nama_jasa = $('#nama_jasa').val();
	var harga_jasa = $('#harga_jasa').val();

	if(kode_jasa==''){
		swal("","Kode Jasa masih kosong!").then((value) => {
			$('#kode_jasa').focus();
		});
		return false;
	}
	if(nama_jasa==''){
		swal("","Nama Jasa masih kosong!").then((value) => {
			$('#nama_jasa').focus();
		});
		return false;
	}
	if(harga_jasa==''){
		swal("","Harga Jasa masih kosong!").then((value) => {
			$('#harga_jasa').focus();
		});
		return false;
	}
			
	$.get("data.php?inputJasa=jasa&kode_jasa="+kode_jasa+"&nama_jasa="+nama_jasa+"&harga_jasa="+harga_jasa,
	function(data){
		if(data==1){
			swal("Maaf ! ","Kode jasa sudah ada").then((value) => {
				$('#kode_jasa').focus();
				$('#kode_jasa').select();
			});
			return false;
		}else{
			table.ajax.url( ajaxData ).load();
			$('#EditPost').modal('hide');
			swal(
			{
			title: 'Sukses!',
			text: 'Data berhasil ditambahkan',
			type: 'success',
			timer: 2000
			}
			);
		}
	});
});

$( "#SaveEdit" ).click(function () {
	var id_jasa = $('#id_jasa').val();
	var kode_jasa = $('#kode_jasa').val();
	var nama_jasa = $('#nama_jasa').val();
	var harga_jasa = $('#harga_jasa').val();

	if(kode_jasa==''){
		swal("","Kode Jasa masih kosong!").then((value) => {
			$('#kode_jasa').focus();
		});
		return false;
	}
	if(nama_jasa==''){
		swal("","Nama Jasa masih kosong!").then((value) => {
			$('#nama_jasa').focus();
		});
		return false;
	}
	if(harga_jasa==''){
		swal("","Harga Jasa masih kosong!").then((value) => {
			$('#harga_jasa').focus();
		});
		return false;
	}
$.get("data.php?updateJasa=jasa&id_jasa="+id_jasa+"&kode_jasa="+kode_jasa+"&nama_jasa="+nama_jasa+"&harga_jasa="+harga_jasa,
function(data){
	table.ajax.url( ajaxData ).load();
	$('#EditPost').modal('hide');
	//setTimeout(function() { $('#ModalSukses').modal('show'); }, 1000);
	//setTimeout(function() { $('#ModalSukses').modal('hide'); }, 2000);
	swal(
{  
	title: 'Sukses!',
	text: 'Data berhasil diperbaharui',
	type: 'success',
	timer: 2000
}
	);
});
});

});

$( "#import" ).click(function () {
 $('#modalImport').modal('show');
 $('#fileExcel').val('');


});


$( "#refresh" ).click(function () {
	table.ajax.url( ajaxData ).load();
} );	

function brgID(){
	var imgID=document.getElementById("imgID");
	var b=document.getElementById("kode_jasa");
	var a=""+Math.floor((Math.random()*10000)+1);
	var c=""+Math.floor((Math.random()*10000)+1);
	b.value=""+a+c
	imgID.value=""+a+c
	};

/*
function pilihSatuan(e) {
    document.getElementById("satuan").value = e.target.value;
		$('#selectSatuan').hide();

}
function pilihKategori(e) {
    document.getElementById("kategori_barang").value = e.target.value;
		$('#selectKategori').hide();

}
*/
String.prototype.reverse = function () {
        return this.split("").reverse().join("");
    }

    function reformatText(input) {        
        var x = input.value;
        x = x.replace(/,/g, ""); // Strip out all commas
        x = x.reverse();
        x = x.replace(/.../g, function (e) {
            return e + ",";
        }); // Insert new commas
        x = x.reverse();
        x = x.replace(/^,/, ""); // Remove leading comma
        input.value = x;
    }



</script>