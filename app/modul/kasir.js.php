<script src="<?php echo $CORE_URL;?>/assets/plugins/air-datepicker/js/datepicker.min.js"></script>
<script src="<?php echo $CORE_URL;?>/assets/plugins/air-datepicker/js/i18n/datepicker.id.js"></script>
<script src="<?php echo $CORE_URL;?>/assets/plugins/numpad/jquery.numpad.js"></script>

<style>
.addBarang:focus{border:3px solid blue}
</style>
<script>  
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function formatMoney(number, decPlaces, decSep, thouSep) {
	decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
	decSep = typeof decSep === "undefined" ? "." : decSep;
	thouSep = typeof thouSep === "undefined" ? "," : thouSep;
	var sign = number < 0 ? "-" : "";
	var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
	var j = (j = i.length) > 3 ? j % 3 : 0;

	return sign +
		(j ? i.substr(0, j) + thouSep : "") +
		i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
		(decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}
function bayarKasir(){
	setCookie("namaBundle", "");
	setCookie("diskonBundle", "");
	setCookie("status", "");
	setCookie("isJasa", "");
	$('#num2').focus();
	var nilaiTotal=$('#totalBayar').val();
	var totalGrandKasir=$('#totalGrandKasir').val();
	var totalPlusPajak = parseInt(totalGrandKasir.replace(",", ""))+((parseInt(totalGrandKasir.replace(",", ""))*<?php echo getModul('nominal_pajak');?>)/100);
	if($('#metode3').is(':checked')){
		var persenEdc = $("input[name=checkBank]:checked", "#formCheckBank").val();
		totalPlusPajak = totalPlusPajak + ((totalPlusPajak*persenEdc)/100);
	}
	var num1 = document.getElementById('num1').value;
	var num2 = document.getElementById('num2').value;
	var diskon = document.getElementById('diskon').value;
	var voucher = document.getElementById('voucher').value;
	var pajak = document.getElementById('pajak').value;
if(nilaiTotal<=0 || nilaiTotal==0){
	swal("","Jumlah barang masih kosong!").then(function(value)  {
	});
	return false;
}
	$('#doBayarKasir').modal('show');
	$('#modalGrandKasir').val(totalGrandKasir);
	$('#num1').val(parseInt(totalGrandKasir.replace(",", "")));
	$('#num2').val('');
	$('#subt').val('');
	$('#voucher').val('');
	$('#diskon').val('');
	// $('#pajak').val('');
	$('#ekspedisi').val('');
	$('#ongkir').val('');
	$("#idPelanggan").val('');
	$("#namaPelanggan").val('');
	$( "#subt2" ).load( "data.php?kembaliNol=0" );
	$( "#hitungTotal" ).html('<b>Rp.'+formatMoney(totalPlusPajak, 0)+'</b>');
	$('#totalHidden').val(totalPlusPajak);
	

	
}
var ajaxBarang="data.php?kasirBarang=daftar_barang";
var ajaxPelanggan="data.php?tablePelanggan=pelanggan";
var ajaxData="data.php?tableKasir=kasir_penjualan";
var ajaxBundling="data.php?tableBundlingBarang=bundling_barang";
var ajaxJasa="data.php?tableJasa=jasa";
// var isBundle;
shortcut.add("f1",function() {
$('#doBarang').modal('show');
$('input[type=search]', '').focus();
$('input[type=search]', '').select();

});
shortcut.add("f2",function() {
$('#qty').focus();
});
shortcut.add("f3",function() {
$('#addCart').focus();
});
shortcut.add("end",bayarKasir);


$(document).ready(function() {
<?php //xKasir();?>
$("#TbDibayar").hide();

$('#kodeBarang').focus();

var faktur = $('#faktur').val();
$('#loadUser').load("data.php?loadUser=user");
$('#loadFaktur').load("data.php?loadFaktur=1&type=PJ");
$('#loadDate').load("data.php?loadDate=1");
 $("#showTempo").hide();
//$('[data-toggle="tooltip"]').tooltip(); 


$('#selectSatuan').hide();
$('#selectKategori').hide();
$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );
    var tableBarang = $('#tableBarang').DataTable( {
    "language": {
      "emptyTable": "&lt;  No data available in table &gt;"
    },
		keys: true,
		"pageLength": 8,
		"lengthMenu": [ 5,10, 25, 50 ],
		"paginate":true,
		"bLengthChange": false,
		"info":false,
		"length":false,
        "ajax": ajaxBarang ,
		"order": [[ 0, "desc" ]],
        "columnDefs": [ 
		{
			"targets": [ -1 ],    
			"data": null,
			"render": function ( data, type, row ) {

			
				if(parseInt(data[4])<=parseInt(data[7])){
					btn_pilih="<button href='#' class='btn btn-default btn-sm' id='addBarang' disabled><i class='fa fa-check-square-o'></i> pilih </button>";
				}else{
					btn_pilih="<button href='#' class='btn btn-warning btn-sm' id='addBarang'><i class='fa fa-check-square-o'></i> pilih </button>";
				}
			return btn_pilih;
			},
		},
		{
		"targets": [ 0 ],
		"visible": false,
		"searchable": false
		}
		]
    } );
	tableBarang
	.on('key-focus', function (e, datatable, cell) {
	datatable.rows().deselect();
	datatable.row( cell.index().row ).select();
	});
	

    var tablePelanggan = $('#tablePelanggan').DataTable( {
    "language": {
      "emptyTable": "&lt;  No data available in table &gt;"
    },

		"pageLength": 6,
		"lengthMenu": [ 5,10, 25, 50 ],
		"paginate":true,
		"bLengthChange": false,
		"info":false,
		"length":false,
        "ajax": {
			"type": "GET",
			"url": ajaxPelanggan,
			"dataSrc": function(json){
				for(var i = 0; i < json.data.length; i++){
					json.data[i].splice(5,1);
					if(json.data[i][5] == 1){
						json.data[i][5] = '<p style="font-size:14px;" class="label label-primary">Member</p>';
					}else{
						json.data[i][5] = '<p style="font-size:14px;" class="label label-default">Biasa</p>';
					}
					// json.data[i][6] = json.data[i][6];
				}
				return json.data;
			}
		},
		"order": [[ 0, "desc" ]],
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent": "<a href='#' class='btn btn-warning btn-sm' id='addPelanggan'><i class='fa fa-check-square-o'></i> pilih</a>"
        },
		{
		"targets": [ 0,3 ],
		"visible": false,
		"searchable": false
		}
		]
    } );

	$('#tablePelanggan tbody').on( 'click', '#addPelanggan', function () {
	var data = tablePelanggan.row( $(this).parents('tr') ).data();
	var metode = $('input[name=metode]:checked', '#formKasir').val();
	$('#idPelanggan').val(data[ 0 ]);
	$('#namaPelanggan').val(data[ 1 ]);
	$('#diskon').val(data[6]);
	$('#doPelanggan').modal('hide');
	var diskon = parseInt(data[6]);
	var total = parseInt($('#totalHidden').val());
	var total2 = parseInt($('#totalGrandKasir').val().replace(",", ""));
	total = total - ((total*diskon)/100);
	$( "#hitungTotal" ).html('<b>Rp.'+formatMoney(total, 0)+'</b>');
	$("#num1").val(total2);
	if(metode=='kredit'){
		if(data[6] == 0){
			console.log(data[6]);
			swal("","Pelanggan yang dipilih bukan member").then(function(value){});
			return false
		}
		total = total + ((total*3)/100);
		$( "#hitungTotal" ).html('<b>Rp.'+formatMoney(total, 0)+'</b>');
		$("#num1").val(total2);
	}
	} );
	
	$('#tableBarang tbody').on( 'click', '#addBarang', function () {
	var data = tableBarang.row( $(this).parents('tr') ).data();
		$('#idBarang').val(data[ 0 ]);
		$('#kodeBarang').val(data[ 1 ]);
		$('#namaBarang').val(data[ 2 ]);
		// $('#hargaJual').val(parseInt(data[ 3 ].replace(",", "")) + parseInt((data[ 3 ].replace(",", "")*10)/100));
		$('#hargaJual').val(data[ 3 ]);
		$('#stokBarang').val(data[ 4 ]);
		$('#doBarang').modal('hide');
		$('#qty').focus();
		$('#qty').select();
		
	} );
	
	
    var table = $('#dataTable').DataTable( {
    "language": {
      "emptyTable": "&lt;  No data available in table &gt;"
    },
		scrollY:        '50vh',
		"scrollX": true,
		"pageLength": 5000,
		"paginate":false,
		"bFilter":false,
		"info":false,
		"bLengthChange": false,
		select: true,
        "ajax": {
			"type" : "GET",
			"url" : ajaxData,
			"dataSrc" : function(data){
				var tmp = data.data;
				tmp.forEach(function(row){
					if(row[9] == 3){
						$('#hiddenBundle').html('<input type="text" id="isBundle" value="1" style="display:none">');
					}
				});
				// $('#headBundle').html('<h1 class="page-header">'+window.namaBundle+'</h1>');
				// console.log("bundling status is the " + $("#isBundle").val());
				$('#header').text(getCookie('namaBundle'));
				return tmp;
			}
		} ,
		"order": [[ 0, "asc" ]],
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent": "<button  class='btn btn-default btn-xs' id='edit'><i class='fa fa-pencil-square-o'></i></button> <button class='btn btn-default btn-xs' id='delete'><i class='fa fa-trash-o'></i></button>"
        },
		{
		"targets": [4 ],
		"render": function ( data, type, row ) {
					var isBundle = $("#isBundle").val();
					if(isBundle == 1){
						var String = data;
					}else{
						var String = '<button id="minus" class="btn btn-xs btn-default"><i class="fa fa-minus"></i></button><span style="width:80px;padding:3px"> ' + data +' </span><button id="plus" class="btn btn-xs btn-default"><i class="fa fa-plus"></i></button>';
					}
                    return String;
                },
		},
		{
		"targets": [5 ],
		"render": function ( data, type, row ) {
					var isBundle = $("#isBundle").val();
					var diskonBundle = getCookie("diskonBundle");
					var String = "";
					if(isBundle == 1){
						String = data + "(potongan harga : " + diskonBundle + "%)";
					}else{
						String = data;
					}
                    return String;
                },
		},
		{
		"targets": [ 0 ],
		"visible": false,
		"searchable": false
		}
		]
    } );
 
    $('#dataTable tbody').on( 'click', '#delete', function () {
		setCookie("namaBundle", "");
		setCookie("diskonBundle", "");
		setCookie("status", "");
		setCookie("isJasa", "");		
        var data = table.row( $(this).parents('tr') ).data();
		$("#isBundle").val(0);
//alert(data[ 0 ]);
	$.get("data.php?delCart="+data[ 0 ]+"&tableCart=kasir_penjualan",
	function(data){
	table.ajax.url( ajaxData ).load();
	$(this).parents('tr').fadeOut(300);
	$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
	$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );
	}
	);
    } );
	
	$('#dataTable tbody').on( 'click', '#edit', function () {
	var data = table.row( $(this).parents('tr') ).data();
		$('#EditCart').modal('show');
		//$('#EditPostLabel').html(data[ 0 ]);
		$('#idCart').val(data[ 0 ]);
		$('#kode_barang').val(data[ 1 ]);
		$('#nama_barang').val(data[ 2 ]);
		$('#hargaCart').val(data[ 3 ]);
		$('#qtyCart').val(data[ 4 ]);
		$('#subTotal').val(data[ 5 ]);
		if(getCookie('isJasa') == 1){
			$('#stok').hide();
		}else{
			$('#stokCart').load( "data.php?stokCart="+data[7] );
		}

		var isBundle = $("#isBundle").val();
		if(isBundle == 1){
			$('#hargaCart').prop("disabled", true);
			$('#qtyCart').prop("disabled", true);
		}

} );
	
	$('#dataTable tbody').on( 'click', '#plus', function () {
	var data = table.row( $(this).parents('tr') ).data();
	$.get("data.php?plusCart="+data[4]+'&id='+data[0]+'&harga='+data[3]+"&modeCart=kasir_penjualan",
	function(data){
		table.ajax.url( ajaxData ).load();
		$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
		$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );
		}
		);

} );
	$('#dataTable tbody').on( 'click', '#minus', function () {
	var data = table.row( $(this).parents('tr') ).data();
	
if(data[4]<=1){
	swal("","Jumlah barang minimum").then(function(value)  {
	});
	return false;
}

	$.get("data.php?minusCart="+data[4]+'&id='+data[0]+'&harga='+data[3]+"&modeCart=kasir_penjualan",
	function(data){
		table.ajax.url( ajaxData ).load();
		$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
		$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );

		}
		);


} );

$( "#showBarang" ).click(function () {
	var isBundle = $('#isBundle').val();
	if(isBundle == 1){
		$('#doBarang').modal('hide');	
		$('#doBundleBarang').modal('show');
		$('#bukanBundle').hide();
		$('#headBundle').text('*tidak dapat memilih barang, selesaikan transaksi bundling product terlebih dahulu');
		tableBoundle.ajax.url( ajaxBundling ).load();
	}else{
		$('#doBarang').modal('show');
		$('input[type=search]', '').select();
		$('input[type=search]', '').select();
	}
	

	tableBarang.ajax.url( ajaxBarang ).load();
} );

//Jasa dari sini
$("#jasa").click(function(){
	$('#doBarang').modal('hide');
	$('#doJasa').modal('show');
	tableJasa.ajax.url( ajaxJasa ).load();
});
var tableJasa = $("#tableJasa").DataTable({
		"language": {
			"emptyTable": "No data available in table"
		},
		responsive: true,
		ordering: true,
		"pageLength": 20,
		"paginate":true,
		"filter":true,
		"info":true,
		"length":false,
        "ajax": ajaxJasa,
		"bLengthChange": false,
		// "order": [[ 3, "desc" ]],
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent": "<button href='#' class='btn btn-warning btn-sm' id='addJasa'><i class='fa fa-check-square-o'></i> pilih </button>"
        },
		{
			"targets" : 0,
			"visible": false,
			"searchable": false
		}]
	});

	$('#tableJasa tbody').on( 'click', '#addJasa', function () {
		var data = tableJasa.row( $(this).parents('tr') ).data();
		setCookie("isJasa",1);
		$('#idBarang').val(data[0]);
		$('#kodeBarang').val(data[1]);
		$('#namaBarang').val(data[2]);
		$('#hargaJual').val(data[3]);
		$('#qty').focus();
		$('#qty').select();
		$('#doJasa').modal('hide');
	});
//Jasa smpe sini

//Bundle dari sini
$("#bundle").click(function(){
	$('#doBarang').modal('hide');
	$('#doJasa').modal('hide');
	$('#doBundleBarang').modal('show');
	tableBoundle.ajax.url( ajaxBundling ).load();
});
var tableBoundle = $("#tableBundleBarang").DataTable({
		"language": {
			"emptyTable": "No data available in table"
		},
		responsive: true,
		ordering: true,
		"pageLength": 20,
		"paginate":true,
		"filter":true,
		"info":true,
		"length":false,
        "ajax": {
			"type" : "GET",
			"url" : ajaxBundling,
			"dataSrc" : function(data){
				for(var i = 0; i < data.data.length; i++){
					data.data[i].splice(0,1);
				}
				return data.data;
			}
		},
		"bLengthChange": false,
		"order": [[ 3, "desc" ]],
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent": "<button href='#' class='btn btn-warning btn-sm' id='addBundle'><i class='fa fa-check-square-o'></i> pilih </button>"
        } ]
	});

$('#tableBundleBarang tbody').on( 'click', '#addBundle', function () {
	var data = tableBoundle.row( $(this).parents('tr') ).data();
	// var kodeBarang = data[1].split(", ");
	var ids_barang_utama = data[1];
	var ids_barang_bonus = data[3];
	$('#idBarang').val('bundle');
	$('#headB').show();
	window.namaBundle = data[0];
	setCookie("namaBundle", data[0]);
	$('#namaBundle').val(data[0]);
	$('#kodeBarangUtama').val(ids_barang_utama);
	$('#kodeBarangBonus').val(ids_barang_bonus);
	$('#namaBarangUtama').val(data[2]);
	$('#namaBarangBonus').val(data[4]);
	window.diskonBundle = data[5]; 
	setCookie("diskonBundle", data[5]);
	setCookie("status", 3);
	$('#hargaJual').val(data[5]);
	$('#qty').focus();
	$('#qty').select();
	// console.log(ids_barang_utama);
	$('#kodeBarang').val('-');
	$('#namaBarang').val(data[0])
	for(var i = 0; i < data[6]; i++){
		$.get("data.php?pilihBarang="+ids_barang_utama, function(response){
			if(response != 0){
				
			}
		});
	}
	for(var i = 0; i < data[7]; i++){
		$.get("data.php?pilihBarang="+ids_barang_bonus, function(response){
			if(response != 0){
				
			}
		});
	}
	$('#doBundleBarang').modal('hide');
	// 	// $('#hargaJual').val(parseInt(data[ 3 ].replace(",", "")) + parseInt((data[ 3 ].replace(",", "")*10)/100));
	// 	$('#hargaJual').val(data[ 3 ]);
	// 	$('#stokBarang').val(data[ 4 ]);
	// 	$('#doBarang').modal('hide');
	// 	$('#qty').focus();
	// 	$('#qty').select();
});

$("#bukanBundle").click(function(){
	$('#doBarang').modal('show');
	$('#doBundleBarang').modal('hide');
});
//Smpe sini bundle


$( "#newKasir" ).click(function () {
	$('#modalPrint').modal('hide');
	table.ajax.url( ajaxData ).load();
	$('#loadFaktur').load("data.php?loadFaktur=1&type=PJ");
	$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
	$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );
	//$( "#loadBody" ).load( "load.php?mode=kasir" );
	$("#date").val("<?php echo date("d/m/Y");?>");
} );

$( "#showPelanggan" ).click(function () {
	$('#doPelanggan').modal('show');
	$('#nama_pelanggan').focus();
	//tableBarang.ajax.url( ajaxBarang ).load();
} );
$( "#bayarKasir" ).click(bayarKasir);

$('#addCartForm').submit(function(e) {
	
var user_id = $('#user_id').val();
var id_barang = $('#idBarang').val();
var kode_barang = $('#kodeBarang').val();
var nama_barang = $('#namaBarang').val();
var stok_barang = $('#stokBarang').val();
var harga = $('#hargaJual').val();
var faktur = $('#faktur').val();
var qty = $('#qty').val();

if(kode_barang=='' || kode_barang==0){
	swal("","Tambahkan Kode Barang").then(function(value) {
		$('#kodeBarang').focus();
	});
	return false;
}
	
if(parseInt(stok_barang) < parseInt(qty)){
	//alert('Maaf! Stok barang tidak mencukupi ( sisa : '+ stok_barang +' )');
	swal("","Maaf! Stok barang tidak mencukupi ( sisa : "+ stok_barang +" )").then(function(value) {
			$('#qty').focus();
	});

	return false;
	
}

e.preventDefault();
$.ajax({
url: "data.php",
type: "POST",
data:  new FormData(this),
contentType: false,
cache: false,
processData:false,
success: function(data)
{
table.ajax.url( ajaxData ).load();
$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );
$('#idBarang').val('');
$('#kodeBarang').val('');
$('#namaBarang').val('');
$('#stokBarang').val('');
$('#hargaJual').val('');
$('#ekspedisi').val('');
$('#ongkir').val('');
$('#qty').val(1);
$('#kodeBarang').focus();
//$( "#loadBody" ).load( "load.php?mode=kasir" );

},
error: function() 
{} 	        
});
});

$( "#addCart" ).click(function () {
	var isBundle = false;
	if($('#idBarang').val() == "bundle"){
		var user_id = $('#user_id').val();
		var id_barang = $('#idBarang').val();
		var nama_bundle = $('#namaBundle').val();
		var kode_barang_utama = $('#kodeBarangUtama').val();
		var kode_barang_bonus = $('#kodeBarangBonus').val();
		var nama_barang_utama = $('#namaBarangUtama').val();
		var nama_barang_bonus = $('#namaBarangBonus').val();
		var stok_barang = $('#stokBarang').val();
		var harga = $('#hargaJual').val();
		var faktur = $('#faktur').val();
		var qty = $('#qty').val();
		var arrKode1 = kode_barang_utama.split(", ");
		var arrKode2 = kode_barang_bonus.split(", ");
		var arrNama1 = nama_barang_utama.split(", ");
		var arrNama2 = nama_barang_bonus.split(", ");
		arrKode1.forEach(function(row, index){
			$.get("data.php?addCart=kasir_penjualan&id_barang="+id_barang+"&kode_barang="+row+"&nama_barang="+arrNama1[index]+"&harga="+harga+"&qty="+qty+"&faktur="+faktur+"&user_id="+user_id+"&nama_bundle="+nama_bundle,
				function(data){
				$('#idBarang').val('');
				$('#kodeBarang').val('');
				$('#namaBarang').val('');
				$('#stokBarang').val('');
				$('#hargaJual').val('');
				$('#ekspedisi').val('');
				$('#ongkir').val('');
				$('#qty').val(1);
				$('#kodeBarang').focus();
				$( "#totalSum" ).html();
				$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
				$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );
			});
		});
		arrKode2.forEach(function(row, index){
			$.get("data.php?addCart=kasir_penjualan&id_barang="+id_barang+"&kode_barang="+row+"&nama_barang="+arrNama2[index]+"&harga="+harga+"&qty="+qty+"&faktur="+faktur+"&user_id="+user_id+"&nama_bundle="+nama_bundle,
				function(data){
				$('#idBarang').val('');
				$('#kodeBarang').val('');
				$('#namaBarang').val('');
				$('#stokBarang').val('');
				$('#hargaJual').val('');
				$('#ekspedisi').val('');
				$('#ongkir').val('');
				$('#qty').val(1);
				$('#kodeBarang').focus();
				$( "#totalSum" ).html();
				$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
				$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );
			});
		});
		table.ajax.url( ajaxData ).load();
		isBundle = true;

	}else if(getCookie("isJasa") == 1){
		var user_id = $('#user_id').val();
		var id_barang = $('#idBarang').val();
		var nama_bundle = $('#namaBundle').val();
		var kode_barang = $('#kodeBarang').val();
		var nama_barang = $('#namaBarang').val();
		var stok_barang = $('#stokBarang').val();
		var harga = $('#hargaJual').val();
		var faktur = $('#faktur').val();
		var qty = $('#qty').val();

		$.get("data.php?addCart=kasir_penjualan&id_barang="+id_barang+"&kode_barang="+kode_barang+"&nama_barang="+nama_barang+"&harga="+harga+"&qty="+qty+"&faktur="+faktur+"&user_id="+user_id+"&isJasa=1",
				function(data){
				$('#idBarang').val('');
				$('#kodeBarang').val('');
				$('#namaBarang').val('');
				$('#stokBarang').val('');
				$('#hargaJual').val('');
				$('#ekspedisi').val('');
				$('#ongkir').val('');
				$('#qty').val('');
				$('#kodeBarang').focus();
				$( "#totalSum" ).html();
				$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
				$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );
			});
	}
var user_id = $('#user_id').val();
var id_barang = $('#idBarang').val();
var kode_barang = $('#kodeBarang').val();
var nama_barang = $('#namaBarang').val();
var stok_barang = $('#stokBarang').val();
var harga = $('#hargaJual').val();
var faktur = $('#faktur').val();
var qty = $('#qty').val();

if(kode_barang=='' || kode_barang==0){
	swal("","Tambahkan Kode Barang").then(function(value) {
		$('#kodeBarang').focus();
	});
	return false;
}
	
if(parseInt(stok_barang) < parseInt(qty)){
	//alert('Maaf! Stok barang tidak mencukupi ( sisa : '+ stok_barang +' )');
	swal("","Maaf! Stok barang tidak mencukupi ( sisa : "+ stok_barang +" )").then(function(value) {
			$('#qty').focus();
	});

	return false;
	
}else{
	if(isBundle == false){
		$.get("data.php?addCart=kasir_penjualan&id_barang="+id_barang+"&kode_barang="+kode_barang+"&nama_barang="+nama_barang+"&harga="+harga+"&qty="+qty+"&faktur="+faktur+"&user_id="+user_id,
		function(data){
			table.ajax.url( ajaxData ).load();
			$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
			$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );
			$('#idBarang').val('');
			$('#kodeBarang').val('');
			$('#namaBarang').val('');
			$('#stokBarang').val('');
			$('#hargaJual').val('');
			$('#ekspedisi').val('');
			$('#ongkir').val('');
			$('#qty').val(1);
			$('#kodeBarang').focus();
	
			}
			);
	}
}
			
} );

$( "#addPelanggan" ).click(function () {
var nama_pelanggan = $('#nama_pelanggan').val();
var alamat = $('#alamat').val();
var no_hp = $('#no_hp').val();
var isMember = $('input[name=member]:checked', '#member').val();
var diskonMember = $("#diskonMember").val();

if(nama_pelanggan==''){
		swal("","Masukkan Nama Pelanggan").then(function(value) {
			$('#nama_pelanggan').focus();
		});
		return false;
	}
	
$.get("data.php?inputPelanggan=pelanggan&nama_pelanggan="+nama_pelanggan+"&alamat="+alamat+"&no_hp="+no_hp+"&member="+isMember+"&diskon_member="+diskonMember,
function(data){
	tablePelanggan.ajax.url( ajaxPelanggan ).load();
	$('#EditPost').modal('hide');
	$('#nama_pelanggan').val('');
	$('#alamat').val('');
	$('#no_hp').val('');
	$('input[name=member]:checked', '#member').val('');
	$("#diskonMember").val('');
	}
);
		
} );

$( "#inputBayar" ).click(function () {
var faktur = $('#faktur').val();
var user_id = $('#user_id').val();
var pelanggan_id = $('#idPelanggan').val();
var date = $('#date').val();
var total = $('#num1').val();
var dibayar = $('#num2').val();
var voucher = $('#voucher').val();
var diskon = $('#diskon').val();
var pajak = $('#pajak').val();
var ongkir = $('#ongkir').val();
var ekspedisi = $('#ekspedisi').val();
var metode = $('input[name=metode]:checked', '#formKasir').val();
var tempo = $('#tempo').val();
var metode=$('input[name=metode]:checked', '#formKasir').val();
var debitNumber = $('#debitNumber').val();
var status = (getCookie("status") != "") ? getCookie("status") : 1;
var persenEdc = $("input[name=checkBank]:checked", "#formCheckBank").val();
setCookie("namaBundle", "");
setCookie("diskonBundle", "");
setCookie("status", "");
setCookie("isJasa", "");
$('#isBundle').val(0);
if(metode=='pre_order'){

	
	$.get("data.php?inputBayar=bayar&status="+status+"&faktur="+faktur+"&pelanggan_id="+pelanggan_id+"&date="+date+"&total="+total+"&voucher="+voucher+"&diskon="+diskon+"&dibayar=0&metode="+metode+"&tempo=0&user_id="+user_id+"&pajak="+pajak+"&ongkir="+ongkir+"&ekspedisi="+ekspedisi,
	function(data){
		//location.reload();	
		table.ajax.url( ajaxData ).load();
		$('#doBayarKasir').modal('hide');
		$('#dataPrint').load("data.php?printKasir=bayar&faktur="+faktur);	
		$('#modalPrint').modal('show');
		//slocation.reload();
		$("#idPelanggan").val('');
		$("#namaPelanggan").val('');
		$("#frame").hide();
		$("#printKasir").show();
		$('#dataPrint').show();
	}
	);	


}else if(metode=='tunai'){

	if(dibayar=='' || dibayar==null || dibayar==0){
		//alert("Masukkan Nama Pelanggan!");
		swal("","Masukkan Pembayaran").then(function(value) {
			$('#num2').focus();
		});
		
		return false;
	}
	
	var bayarKurang = $('#bayarKurang').val();
	if(bayarKurang==1){
		//alert("Masukkan Nama Pelanggan!");
		swal("","Pembayaran Kurang").then(function(value){
			$('#num2').focus();
			$('#num2').select();
		});
		
		return false;
	}
	
	$.get("data.php?inputBayar=bayar&status="+status+"&faktur="+faktur+"&pelanggan_id="+pelanggan_id+"&date="+date+"&total="+total+"&voucher="+voucher+"&diskon="+diskon+"&dibayar="+dibayar+"&metode="+metode+"&tempo="+tempo+"&user_id="+user_id+"&pajak="+pajak+"&ongkir="+ongkir+"&ekspedisi="+ekspedisi,
	function(data){
		//location.reload();	
		table.ajax.url( ajaxData ).load();
		$('#doBayarKasir').modal('hide');
		$('#dataPrint').load("data.php?printKasir=bayar&faktur="+faktur);	
		$('#modalPrint').modal('show');
		//slocation.reload();
		$("#idPelanggan").val('');
		$("#namaPelanggan").val('');
		$("#frame").hide();
		$("#printKasir").show();
		$('#dataPrint').show();

	}
	);	


}else if(metode=='kredit'){
	if(pelanggan_id=='' || pelanggan_id==null || pelanggan_id==0){
		//alert("Masukkan Nama Pelanggan!");
		swal("","Pilih Nama Pelanggan").then(function(value) {
			$('#namaPelanggan').focus();
		});
		
		return false;
	}
	else if(tempo=='' || tempo==null || tempo==0){
		
		$('#tempo').focus()
		return false;
	}
	else{
	
	$.get("data.php?inputBayar=bayar&status="+status+"&faktur="+faktur+"&pelanggan_id="+pelanggan_id+"&date="+date+"&total="+total+"&voucher="+voucher+"&diskon="+diskon+"&dibayar="+dibayar+"&metode="+metode+"&tempo="+tempo+"&user_id="+user_id+"&pajak="+pajak,
	function(data){
		//location.reload();	
		table.ajax.url( ajaxData ).load();
		$('#doBayarKasir').modal('hide');
		$('#dataPrint').load("data.php?printKasir=bayar&faktur="+faktur);	
		$('#modalPrint').modal('show');
		$('#tempo').val('');
		$("#num2").prop('disabled', false);	 
		$("#metode1").prop('checked', true);
		$("#metode2").prop('checked', false);
		$("#metode3").prop('checked', false);
		$("#showTempo").hide();
		$("#idPelanggan").val('');
		$("#namaPelanggan").val('');
		$("#frame").hide();
		$("#printKasir").show();
		$('#dataPrint').show();

		//slocation.reload();
	}
	);	
	}
}else if(metode=='debit'){
	if(debitNumber=='' || debitNumber==null || debitNumber==0){
		swal("","Masukkan no. kartu").then(function(value) {
			$('#debitNumber').focus();
		});
		return false;
	}
	else{

		$.get("data.php?inputBayar=bayar&status="+status+"&faktur="+faktur+"&pelanggan_id="+pelanggan_id+"&date="+date+"&total="+total+"&voucher="+voucher+"&diskon="+diskon+"&dibayar="+dibayar+"&metode="+metode+"&user_id="+user_id+"&debitNumber="+debitNumber+"&pajak="+pajak+"&pjkEdc="+persenEdc,
		function(data){
			//location.reload();	
			table.ajax.url( ajaxData ).load();
			$('#doBayarKasir').modal('hide');
			$('#dataPrint').load("data.php?printKasir=bayar&faktur="+faktur);	
			$('#modalPrint').modal('show');
			$('#tempo').val('');
			$("#num2").prop('disabled', false);	 
			$("#metode1").prop('checked', true);
			$("#metode2").prop('checked', false);
			$("#metode3").prop('checked', false);
			$("#showTempo").hide();
			$("#idPelanggan").val('');
			$("#namaPelanggan").val('');
			$("#frame").hide();
			$("#printKasir").show();
			$('#dataPrint').show();

			//slocation.reload();
		});	
	}
}


} );

$( "#printKasir" ).click(function () {
	//jQuery('#printArea').print()
	$("#printKasir").hide();
	$("#frame").show();
	$('#dataPrint').hide();
	var faktur = $('#faktur').val();
	var ukuran = $('#ukuran').val();
	var font_family = $('#font_family').val();
	var font_size = $('#font_size').val();
    $("#frame").attr("src", "nota.php?faktur="+faktur+"&ukuran="+ukuran+"&font_family="+font_family+"&font_size="+font_size);

	//window.open("nota.php?faktur="+faktur+"&ukuran="+ukuran+"&font_family="+font_family+"&font_size="+font_size," ", "width=500,height=450"); 
	
	$('#modalPrint').modal('hide');
	//$('#loadDate').load("data.php?loadDate=1");
	$('#loadFaktur').load("data.php?loadFaktur=1&type=PJ");
	$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
	$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );
	$("#date").val("<?php echo date("d/m/Y");?>");
	
	//$( "#loadBody" ).load( "load.php?mode=kasir" );
	
} );
$( "#printSuratJalan" ).click(function () {
	//jQuery('#printArea').print()
	$("#printKasir").hide();
	$("#frame").show();
	$('#dataPrint').hide();
	var faktur = $('#faktur').val();
	var ukuran = $('#ukuran').val();
	var font_family = $('#font_family').val();
	var font_size = $('#font_size').val();
    $("#frame").attr("src", "suratJalan.php?faktur="+faktur+"&ukuran="+ukuran+"&font_family="+font_family+"&font_size="+font_size);

	//window.open("nota.php?faktur="+faktur+"&ukuran="+ukuran+"&font_family="+font_family+"&font_size="+font_size," ", "width=500,height=450"); 
	
	$('#modalPrint').modal('hide');
	//$('#loadDate').load("data.php?loadDate=1");
	$('#loadFaktur').load("data.php?loadFaktur=1&type=PJ");
	$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
	$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );
	$("#date").val("<?php echo date("d/m/Y");?>");
	
	//$( "#loadBody" ).load( "load.php?mode=kasir" );
	
} );

$( "#SaveCart" ).click(function () {
var id = $('#idCart').val();
var qty = $('#qtyCart').val();
var harga = $('#hargaCart').val();
//alert(harga);

$.get("data.php?updateCart=kasir_penjualan&id="+id+"&qty="+qty+"&harga="+harga,
function(data){
	table.ajax.url( ajaxData ).load();
	$('#EditCart').modal('hide');
	$( "#totalSum" ).load( "data.php?totalSum=kasir_penjualan" );
	$( "#totalSumBayar" ).load( "data.php?totalSumBayar=kasir_penjualan" );
}
);		
} );

$( "#refresh" ).click(function () {
	table.ajax.url( ajaxData ).load();
});	



} );

	String.prototype.reverse = function () {
			return this.split("").reverse().join("");
		}

function reformatText(input) {
	var x = input.value;
	x = x.replace(/,/g, ""); // Strip out all commas
	x = x.reverse();
	x = x.replace(/.../g, function (e) {
		return e + ",";
	}); // Insert new commas
	x = x.reverse();
	x = x.replace(/^,/, ""); // Remove leading comma
	input.value = x;
}
function formatValue(input) {
	
	var ongkir = document.getElementById('ongkir').value;
	var num1 = document.getElementById('num1').value;
	var num2 = document.getElementById('num2').value;
	var diskon = document.getElementById('diskon').value;
	var voucher = document.getElementById('voucher').value;
	var pajak = document.getElementById('pajak').value;
	console.log(num1);
	console.log(num2);
	var result = parseInt(num2) - parseInt(num1);
	$( "#hitungTotal" ).load( "data.php?hitungTotal="+result+"&total="+num1+"&diskon="+diskon+"&voucher="+voucher+"&pajak="+pajak+"&ongkir="+ongkir );

	if (!isNaN(result)) {
	document.getElementById('subt').value = result;
	$( "#subt2" ).load( "data.php?hitungKembali="+result+"&total="+num1+"&bayar="+num2+"&diskon="+diskon+"&voucher="+voucher+"&pajak="+pajak+"&ongkir="+ongkir );
	}
	var x = input.value;
	x = x.replace(/,/g, ""); // Strip out all commas
	x = x.reverse();
	x = x.replace(/.../g, function (e) {
		return e + ",";
	}); // Insert new commas
	x = x.reverse();
	x = x.replace(/^,/, ""); // Remove leading comma
	input.value = x;	
}

$('#formKasir input').on('change', function(e) {
	var metode=$('input[name=metode]:checked', '#formKasir').val();
	var totalPajak;
	var totalPlusEdc;
   //alert($('input[name=metode]:checked', '#formKasir').val()); 
   if(metode == 'pre_order'){
	   $("#showTempo").hide();
	   $("#showEdc").hide();
	   $("#TbDibayar").hide();
		$("#num2").prop('disabled', true);	 
		$("#num2").val('');
		$('#debitNumber').val('');
		var totalAwal=$('#totalGrandKasir').val();
		var totalGrandKasir=$('#totalGrandKasir').val();
		totalGrandKasir = parseInt(totalGrandKasir.replace(",", ""));
		var diskon = $('#diskon').val();
		if(diskon != 0 || diskon != ''){
			totalGrandKasir = totalGrandKasir - ((totalGrandKasir*diskon)/100)
		}
		totalPajak = totalGrandKasir + ((totalGrandKasir*<?php echo getModul('nominal_pajak');?>)/100);
		$('#modalGrandKasir').val(totalAwal);
		$( "#hitungTotal" ).html('<b>Rp.'+formatMoney(totalPajak, 0)+'</b>');		
   }else if(metode == 'kredit'){
	   $("#showTempo").show();
	   $("#showEdc").hide();
		$("#num2").prop('disabled', true);	 
		$("#num2").val('');
		$('#debitNumber').val('');
		var totalAwal=$('#totalGrandKasir').val();
		var totalGrandKasir=$('#totalGrandKasir').val();
		totalGrandKasir = parseInt(totalGrandKasir.replace(",", ""));
		var diskon = $('#diskon').val();
		if(diskon != 0 || diskon != ''){
			totalGrandKasir = totalGrandKasir - ((totalGrandKasir*diskon)/100)
		}
		totalPajak = totalGrandKasir + ((totalGrandKasir*<?php echo getModul('nominal_pajak');?>)/100);
		totalPajak = totalPajak + ((totalPajak*3)/100);
		$('#modalGrandKasir').val(totalAwal);
		$( "#hitungTotal" ).html('<b>Rp.'+formatMoney(totalPajak,0)+'</b>');		
   }else if(metode == 'debit'){
		var totalAwal=$('#totalGrandKasir').val();
		var totalGrandKasir=$('#totalGrandKasir').val();
		totalGrandKasir = parseInt(totalGrandKasir.replace(",", ""));
		var diskon = $('#diskon').val();
		if(diskon != 0 || diskon != ''){
			totalGrandKasir = totalGrandKasir - ((totalGrandKasir*diskon)/100)
		}
		totalPajak = totalGrandKasir + ((totalGrandKasir*<?php echo getModul('nominal_pajak');?>)/100);
		var persenEdc = $("input[name=checkBank]:checked", "#formCheckBank").val();
		$("#bankSama").click(function(){
			var persenEdc = $('#bankSama').val();
			totalPlusEdc = ((totalPajak*persenEdc)/100);
			var totalPlusPajakEdc = parseInt(totalPajak) + parseInt(totalPlusEdc);
			$('#modalGrandKasir').val(totalAwal);
			$( "#hitungTotal" ).html('<b>Rp.'+formatMoney(totalPlusPajakEdc,0)+'</b>');		
			$("#showTempo").hide();
			$("#showEdc").show();
			$("#num2").prop('disabled', true);	 
			$("#num2").val(totalPlusPajakEdc);
		});
		$("#bankBeda").click(function(){
			var persenEdc = $('#bankBeda').val();
			totalPlusEdc = ((totalPajak*persenEdc)/100);
			var totalPlusPajakEdc = parseInt(totalPajak) + parseInt(totalPlusEdc);
			$('#modalGrandKasir').val(totalAwal);
			$( "#hitungTotal" ).html('<b>Rp.'+formatMoney(totalPlusPajakEdc,0)+'</b>');		
			$("#showTempo").hide();
			$("#showEdc").show();
			$("#num2").prop('disabled', true);	 
			$("#num2").val(totalPlusPajakEdc);
		});
		totalPlusEdc = ((totalPajak*persenEdc)/100);
		var totalPlusPajakEdc = parseInt(totalPajak) + parseInt(totalPlusEdc);
		$('#modalGrandKasir').val(totalAwal);
		$( "#hitungTotal" ).html('<b>Rp.'+formatMoney(totalPlusPajakEdc,0)+'</b>');		
	   	$("#showTempo").hide();
		$("#showEdc").show();
		$("#num2").prop('disabled', true);	 
		$("#num2").val(totalPlusPajakEdc);
   }
   else{
	   $("#TbDibayar").show();
	    $("#showTempo").hide();
		$("#showEdc").hide();
		$('#tempo').val('');
		$('#debitNumber').val('');
		$("#num2").prop('disabled', false);	   
		$("#num2").val('');
		var totalAwal=$('#totalGrandKasir').val();
		var totalGrandKasir=$('#totalGrandKasir').val();
		totalGrandKasir = parseInt(totalGrandKasir.replace(",", ""));
		var diskon = $('#diskon').val();
		if(diskon != 0 || diskon != ''){
			totalGrandKasir = totalGrandKasir - ((totalGrandKasir*diskon)/100)
		}
		totalPajak = totalGrandKasir + ((totalGrandKasir*<?php echo getModul('nominal_pajak');?>)/100);
		$('#modalGrandKasir').val(totalAwal);
		$( "#hitungTotal" ).html('<b>Rp.'+formatMoney(totalPajak,0)+'</b>');
   }   
   e.preventDefault();
});

</script>