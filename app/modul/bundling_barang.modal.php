<div class="modal fade" id="EditPost" tabindex="-1" role="dialog" aria-labelledby="EditPostLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display:block">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="EditPostLabel"> <i class="fa fa-check-square-o" aria-hidden="true"></i> Data
                    Barang</h4>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <span style="display:none"><input style="width:400px" class="form-control" type="text"
                                id="id_barang"></span>
                        <table class="table">
                            <tr>
                                <td style="width:150px">Nama Bundling</td>
                                <td style="width:10px">:</td>
                                <td>
                                    <input class="form-control" type="text" id="nama_bundle"
                                        placeholder="Promo satu set perlengkapan kucing...">
                                </td>
                            </tr>
                            <tr>
                                <td>Pilih Barang Utama</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" id="ids_barang_utama" style="display:none;">
                                            <textarea class="form-control" rows="2" id="nama_barang_utama" readonly></textarea>
                                            <span class="input-group-btn" title="Pilih Barang" id="pilihBarangUtama">
                                                <button class="btn btn-default" style="height:55px;"><i class="fa fa-cubes"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Pilih Barang Bonus</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" id="ids_barang_bonus" style="display:none;">
                                            <input type="text" id="harga_barang_bonus" style="display:none">
                                            <textarea class="form-control" rows="2" id="nama_barang_bonus" readonly></textarea>
                                            <span class="input-group-btn" title="Pilih Barang" id="pilihBarangBonus">
                                                <button class="btn btn-default" style="height:55px;"><i class="fa fa-cubes"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Diskon Bundle</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <input type="text" id="diskonBundle" class="form-control">
                                        <!-- <div class="input-group">
                                            
                                            <span class="input-group-btn">
                                                <button class="btn btn-default">%</button>
                                            </span>
                                        </div> -->
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Jumlah Barang Utama</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" id="jumlahUtama" style="padding:6px 12px;">
                                        <!-- <div class="input-group">
                                            
                                            <span class="input-group-btn">
                                                <button class="btn btn-default">%</button>
                                            </span>
                                        </div> -->
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Jumlah Barang Bonus</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" id="jumlahBonus" style="padding:6px 12px;">
                                        <!-- <div class="input-group">
                                            
                                            <span class="input-group-btn">
                                                <button class="btn btn-default">%</button>
                                            </span>
                                        </div> -->
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- <div class="col-lg-4">
                        <div id="loadUploader"></div>
                    </div> -->
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" href="#" id="SaveEdit"><i class="fa fa-check-square-o"
                        aria-hidden="true"></i> Update</a>
                <a class="btn btn-primary" href="#" id="SaveInput"><i class="fa fa-check-square-o"
                        aria-hidden="true"></i> Simpan</a>
                <a class="btn btn-warning" href="#" id="reset"><i class="fa fa-retweet" aria-hidden="true"></i>
                    Reset</a>
                <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-window-close"
                        aria-hidden="true"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pilihBarangUtamaModal" tabindex="-1" role="dialog" aria-labelledby="Pilih Barang" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h5 class="modal-title" id="EditPostLabel"><i class="fa fa-check-square-o" aria-hidden="true"></i> Data
                    Barang</h5>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover " id="tableBarangUtama" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width:50px">ID</th>
                                <th>Kode</th>
                                <th>Nama Barang</th>
                                <th>Harga Jual</th>
                                <th>Stok</th>
                                <th style="width:20px">#</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <button class="btn btn-primary text-right" id="tambahBarangUtama">Tambah Barang</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pilihBarangBonusModal" tabindex="-1" role="dialog" aria-labelledby="Pilih Barang" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h5 class="modal-title" id="EditPostLabel"><i class="fa fa-check-square-o" aria-hidden="true"></i> Data
                    Barang</h5>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover " id="tableBarangBonus" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width:50px">ID</th>
                                <th>Kode</th>
                                <th>Nama Barang</th>
                                <th>Harga Jual</th>
                                <th>Stok</th>
                                <th style="width:20px">#</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <button class="btn btn-primary text-right" id="tambahBarangBonus">Tambah Barang</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="EditSatuan" tabindex="-1" role="dialog" aria-labelledby="EditPostLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-primary" style="display:block">
                <h5 class="modal-title" id="EditPostLabel">Satuan Barang</h5>
            </div>
            <div class="modal-body">

                <table class="table table-stripped table-hover" id="tableSatuan" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width:30px">ID</th>
                            <th>Satuan</th>
                            <th style="width:40px!important"></th>
                        </tr>
                    </thead>
                </table>
            </div>

            <div class="modal-footer">
                <span class="pull-left ">Tambah Satuan : </span><input class="" id="inputSatuan" placeholder="satuan">
                <button class="btn btn-warning btn-sm" type="button" id="insertSatuan"><i class="fa fa-plus"
                        aria-hidden="true"></i> Tambah</button>
                <button class="btn btn-default btn-sm" type="button" data-dismiss="modal"><i class="fa fa-window-close"
                        aria-hidden="true"></i> Tutup</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="EditKategori" tabindex="-1" role="dialog" aria-labelledby="EditPostLabel"
    aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-primary" style="display:block">
                <h5 class="modal-title" id="EditPostLabel">Kategori Barang</h5>
            </div>
            <div class="modal-body">
                <div>

                    <table class="table table-stripped table-hover" id="tableKategori" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width:30px">ID</th>
                                <th>Kategori</th>
                                <th style="width:40px!important"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <span class="pull-left ">Tambah Kategori : </span><input class="" id="inputKategori"
                    placeholder="kategori">
                <button class="btn btn-warning btn-sm" type="button" id="insertKategori"><i class="fa fa-plus"
                        aria-hidden="true"></i> Tambah</button>
                <button class="btn btn-default btn-sm" type="button" data-dismiss="modal"><i class="fa fa-window-close"
                        aria-hidden="true"></i> Tutup</button>

            </div>
        </div>
    </div>
</div>

<!-- import -->
<div class="modal fade" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="EditPostLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-primary" style="display:block">
                <h5 class="modal-title" id="EditPostLabel">Import Data Barang</h5>
            </div>
            <div class="modal-body">
                <div class="well">
                    <form id="uploadForm" action="" method="post">
                        <input name="fileExcel" type="file" class="inputFile" id="fileExcel" />
                        <input name="fileTable" type="text" id="fileTable" value="daftar_barang" style="display:none" />
                        <input type="submit" value="Upload Excel " class="btn btn-primary btn-sm"
                            style="margin-top:3px" />
                        <div id="data"></div>
                    </form>
                </div>
                <a href="files/template_barang.xls" class="btn btn-success"><i class="fa fa-file-excel-o"></i>
                    template_barang.xls</a>
                <br />
                <h4>Panduan import data dari Excel :</h4>
                <ol>
                    <li>Download file excel <b>template_barang.xls</b> kemudian edit sesuai data Anda</li>
                    <li>Jika file Excel sudah selesai diedit, upload file tersebut pada form di atas.</li>
                    <li>klik Upload Excel untuk mulai mengupload</li>
                    <li>Proses upload selesai</li>
                </ol>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default btn-sm" type="button" data-dismiss="modal"><i class="fa fa-window-close"
                        aria-hidden="true"></i> Tutup</button>

            </div>
        </div>
    </div>
</div>