<script src="<?php echo $CORE_URL;?>/assets/plugins/air-datepicker/js/datepicker.min.js"></script>
<script src="<?php echo $CORE_URL;?>/assets/plugins/air-datepicker/js/i18n/datepicker.id.js"></script>
<script>
$(document).ready(function() {
    $("#cetakForm").attr("action", "cetak.laporan.php");
    $("#pilihKasir").change(function(){
        $("#cetakForm").attr("action", $("#pilihKasir").val());
        if($("#pilihKasir").val() == ""){
            $("#cetakForm").attr("action", "cetak.laporan.php");
        }
    });
});
</script>