<?php 
$username=$_SESSION['user'];
$userlevel=userLevel($username);
?>
<style>
.bgColor label{
font-weight: bold;
color: #A0A0A0;
}
#targetLayer{
float:left;
width:150px;
height:150px;
text-align:center;
line-height:150px;
font-weight: bold;
color: #C0C0C0;
background-color: #F0E8E0;
border-radius: 4px;
}
#uploadFormLayer{
padding-top:5px
}
.btnSubmit {
	background-color: #696969;
    padding: 5px 30px;
    border: #696969 1px solid;
    border-radius: 4px;
    color: #FFFFFF;
    margin-top: 10px;
}

.image-preview {	
width:150px;
height:150px;
border-radius: 4px;
}
table.dataTable.select tbody tr,
table.dataTable thead th:first-child {
  cursor: pointer;
}
</style>
<script src="<?php echo $CORE_URL;?>/assets/plugins/air-datepicker/js/datepicker.min.js"></script>
<script src="<?php echo $CORE_URL;?>/assets/plugins/air-datepicker/js/i18n/datepicker.id.js"></script>
<script>
var ajaxData="data.php?tableBundlingBarang=bundling_barang";
var ajaxBarang="data.php?kasirBarang=daftar_barang";
var idBundle;
var idBarangPrev;
// var ajaxSatuan="data.php?isBundling=1&tableSatuan=satuan";
// var ajaxKategori="data.php?isBundling=1&tableKategori=kategori_barang";
shortcut.add("f1",function() {
$('#EditPost').modal('show');
});
//$.fn.dataTable.ext.errMode = 'throw';

$(document).ready(function() {
		$("#uploadForm").on('submit',(function(e) {
		var fileExcel=$("#fileExcel").val();

		if(fileExcel==''){
			swal("","File masih kosong!").then((value) => {
				$('#fileExcel').focus();
			});
			return false;
		}
		e.preventDefault();
		$.ajax({
        	url: "import.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
			$("#data").html(data);
			table.ajax.url( ajaxData ).load();

		    },
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
//$('#selectSatuan').hide();
//$('#selectKategori').hide();
//$('[data-toggle="tooltip"]').tooltip(); 
function formatMoney(number, decPlaces, decSep, thouSep) {
	decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
	decSep = typeof decSep === "undefined" ? "." : decSep;
	thouSep = typeof thouSep === "undefined" ? "," : thouSep;
	var sign = number < 0 ? "-" : "";
	var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
	var j = (j = i.length) > 3 ? j % 3 : 0;

	return sign +
		(j ? i.substr(0, j) + thouSep : "") +
		i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
		(decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}
$('#loadUploader').load('data.php?imgUpload=img&imgID=');
    var table = $('#dataTable').DataTable( {
    "language": {
      "emptyTable": "No data available in table"
    },
		  "rowCallback": function(row, data, dataIndex){

      },
	  
		scrollY: '46vh',
		scrollX: true,
		scrollCollapse: true,
		select: true,		
		responsive: true,
		 "ordering": true,
//		bSort: false,
/* 		dom: 'Bfrtip',		

       buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
		*/
		"pageLength": 20,
		"paginate":true,
		"filter":true,
		"info":true,
		"length":false,
        "ajax": {
			"type" : "GET",
			"url" : ajaxData,
			"dataSrc" : function(data){
				// for(var i = 0; i < data.data.length; i++){
				// 	data.data[i].splice(2,1);
				// }
				return data.data;
			}
		},
		"bLengthChange": false,
		"order": [[ 5, "desc" ]],
        "columnDefs": [ {
            "targets": 0,
            "data": null,
            "defaultContent": "<div style='width:60px'><button  <?php displayAkses('barang_edit',$userlevel);?> class='btn btn-default btn-xs' id='edit'><i class='fa fa-pencil-square-o'></i></button> <button <?php displayAkses('barang_hapus',$userlevel);?> class='btn btn-default btn-xs' id='delete'><i class='fa fa-trash-o'></i></button></div>"
        },{
			"targets":6,
			"render": function( data, type, row){
				var String = "Rp."+formatMoney(data,0);
				return String;
			}
		} ],

    } );

	
	$('#dataTable tbody').on( 'click', '#delete', function () {
        var data = table.row( $(this).parents('tr') ).data();


	swal({
	  title: 'Hapus',
	  html: "Anda ingin menghapus data ini? <br> <strong>Nama Bundling </strong>: "+data[ 1 ],
	  type: 'warning',
	  
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Ya, Hapus!'
	}).then((result) => {
	  if (result.value) {
		swal({  
		title: 'Hapus',
		text: 'Data berhasil dihapus',
		type: 'success',
		timer: 2000
	}
	);
			$.get("data.php?deleteBundlingBarang="+data[0],
			function(data){
			table.ajax.url( ajaxData ).load();
			 $(this).parents('tr').fadeOut(300);

			}
			);
  }
})
	
		 //table.ajax.url( 'data.txt' ).load();
    } );
	

		
	$('#dataTable tbody').on( 'click', '#edit', function () {
	var data = table.row( $(this).parents('tr') ).data();
		$('#SaveInput').hide();
		$('#EditPost').modal('show');
		//$('#EditPostLabel').html(data[ 0 ]);
		idBundle = data[0];
		idBarangPrev = data[2];
		console.log(data);
		$('#nama_bundle').val(data[1]);
		$('#ids_barang_utama').val(data[ 2 ]);
		$('#nama_barang_utama').val(data[ 3 ]);
		$('#ids_barang_bonus').val(data[ 4 ]);
		$('#nama_barang_bonus').val(data[ 5 ]);
		$('#diskonBundle').val(data[ 6 ]);
		$('#jumlahUtama').val(data[ 7 ]);
		$('#jumlahBonus').val(data[ 8 ]);

    } );
	
	$( "#pilihKategori" ).click(function () {
			//$('#selectKategori').toggle();
			$('#EditKategori').modal("show");
    } );
	$( "#pilihSatuan" ).click(function () {
			//$('#selectSatuan').toggle();
		$('#EditSatuan').modal("show");

    } );
	
$( "#category" ).click(function () {
var category = $(this).val();

} );

$( "#new" ).click(function () {
	$('#SaveEdit').hide();
	$('#reset').show();
	$('#SaveInput').show();
	$('#EditPost').modal('show');
	$('#nama_bundle').val('');
	$('#ids_barang_utama').val('');
	$("#nama_barang_utama").val('');
	$('#ids_barang_bonus').val('');
	$("#nama_barang_bonus").val('');
	$('#diskonBundle').val('');
	$('#jumlahUtama').val('');
	$('#jumlahBonus').val('');
	$('#loadUploader').load('data.php?imgUpload=img&imgID=0');

} );

var tableBarangUtama = $('#tableBarangUtama').DataTable( {
    "language": {
      "emptyTable": "&lt;  No data available in table &gt;"
    },
		keys: true,
		"pageLength": 8,
		"lengthMenu": [ 5,10, 25, 50 ],
		"paginate":true,
		"bLengthChange": false,
		"info":false,
		"length":false,
        "ajax": ajaxBarang ,
		"order": [[ 0, "desc" ]],
        "columnDefs": [ 
		{
			"targets": [ -1 ],    
			"data": null,
			"render": function ( data, type, row ) {
				// if(parseInt(data[4])<=parseInt(data[7])){
				// 	btn_pilih="<button href='#' class='btn btn-default btn-sm' id='addBarang' disabled><i class='fa fa-check-square-o'></i> pilih </button>";
				// }else{
				// 	btn_pilih="<button href='#' class='btn btn-warning btn-sm' id='addBarang'><i class='fa fa-check-square-o'></i> pilih </button>";
				// }
				btn_pilih = '<input type="checkbox" id="BarangUtama" name="tambahBarang[]" value="'+data[1]+','+data[2]+'">';
			return btn_pilih;
			},
		},
		{
		"targets": [ 0 ],
		"visible": false,
		"searchable": false
		}
		]
    } );

	tableBarangUtama.on('key-focus', function (e, datatable, cell) {
		datatable.rows().deselect();
		datatable.row( cell.index().row ).select();
	});

$("#pilihBarangUtama").click(function(){
	$('#pilihBarangUtamaModal').modal('show');
	tableBarangUtama.ajax.url( ajaxBarang ).load();
})

$("#tambahBarangUtama").click(function(){
	var barang = [];
	var kode = [];
	$('#BarangUtama:checked').each(function(i){
		var tmp = $(this).val().split(",");
		kode[i] = tmp[0]
		barang[i] = tmp[1];
    });
	$("#nama_barang_utama").val(barang.join(", "));
	$("#ids_barang_utama").val(kode.join(", "));
	console.log(barang.join(", "));
	console.log(kode.join(", "));
	$('#pilihBarangUtamaModal').modal('hide');
});

//Batas bonus
var tableBarangBonus = $('#tableBarangBonus').DataTable( {
    "language": {
      "emptyTable": "&lt;  No data available in table &gt;"
    },
		keys: true,
		"pageLength": 8,
		"lengthMenu": [ 5,10, 25, 50 ],
		"paginate":true,
		"bLengthChange": false,
		"info":false,
		"length":false,
        "ajax": ajaxBarang ,
		"order": [[ 0, "desc" ]],
        "columnDefs": [ 
		{
			"targets": [ -1 ],    
			"data": null,
			"render": function ( data, type, row ) {
				// if(parseInt(data[4])<=parseInt(data[7])){
				// 	btn_pilih="<button href='#' class='btn btn-default btn-sm' id='addBarang' disabled><i class='fa fa-check-square-o'></i> pilih </button>";
				// }else{
				// 	btn_pilih="<button href='#' class='btn btn-warning btn-sm' id='addBarang'><i class='fa fa-check-square-o'></i> pilih </button>";
				// }
				btn_pilih = '<input type="checkbox" id="barangBonus" name="tambahBarang[]" value="'+data[1]+','+data[2]+','+data[3]+'">';
			return btn_pilih;
			},
		},
		{
		"targets": [ 0 ],
		"visible": false,
		"searchable": false
		}
		]
    } );

	tableBarangBonus.on('key-focus', function (e, datatable, cell) {
		datatable.rows().deselect();
		datatable.row( cell.index().row ).select();
	});

$("#pilihBarangBonus").click(function(){
	$('#pilihBarangBonusModal').modal('show');
	tableBarangBonus.ajax.url( ajaxBarang ).load();
})

$("#jumlahBonus").change(function(){
	var diskonTmp = $("#diskonBundle").val();
	diskonTmp *= parseInt($("#jumlahBonus").val());
	$("#diskonBundle").val(diskonTmp);
});

$("#tambahBarangBonus").click(function(){
	var barang = [];
	var kode = [];
	var harga = [];
	var diskon = 0;
	$('#barangBonus:checked').each(function(i){
		var tmp = $(this).val().split(",");
		kode[i] = tmp[0]
		barang[i] = tmp[1];
		// harga[i] = tmp[2]+tmp[3];
		diskon += parseInt(tmp[2]+tmp[3]);
    });
	$("#nama_barang_bonus").val(barang.join(", "));
	$("#harga_barang_bonus").val(diskon);
	$("#ids_barang_bonus").val(kode.join(", "));
	$("#diskonBundle").val(diskon);
	console.log(diskon);
	$('#pilihBarangBonusModal').modal('hide');
});

$( "#reset" ).click(function () {

	$('#id_barang').val('');
	$('#kode_barang').val('');
	$('#nama_barang').val('');
	$('#kategori_barang').val('');
	$('#satuan').val('');
	$('#harga_beli').val('');
	$('#harga_jual').val('');
	$('#ukuran').val('');
	$('#warna').val('');
	$('#merek').val('');
	$('#lokasi').val('');
	$('#stok').val('');
	$('#stok_minimal').val('');
	$('#expired').val('');
	$( "#selectSatuan" ).load( "data.php?satuanBarang=satuan" );
	$( "#selectKategori" ).load( "data.php?kategoriBarang=kategori" );
	$('#loadUploader').load('data.php?imgUpload=img&imgID=0');

} );

$( "#SaveInput" ).click(function () {
	$("#EditPost").modal('hide');
	var nama_bundle = $('#nama_bundle').val();
	var nama_barang_utama = $("#nama_barang_utama").val();
	var ids_barang_utama = $('#ids_barang_utama').val();
	var nama_barang_bonus = $("#nama_barang_bonus").val();
	var ids_barang_bonus = $('#ids_barang_bonus').val();
	var diskon_bundle = $('#diskonBundle').val();
	var jumlahUtama = $('#jumlahUtama').val();
	var jumlahBonus = $('#jumlahBonus').val();

	if(nama_bundle==''){
		swal("","Nama Bundling masih kosong!").then((value) => {
			$('#nama_barang').focus();
		});
		return false;
	}
	if(ids_barang_utama==''){
		swal("","Belum ada barang utama yang dipilih!").then((value) => {
			$('#kode_barang').focus();
		});
		return false;
	}
	if(ids_barang_bonus==''){
		swal("","Belum ada barang bonus yang dipilih!").then((value) => {
			$('#kode_barang').focus();
		});
		return false;
	}
	// if(diskon_bundle==''){
	// 	swal("","Harga Bundling masih kosong!").then((value) => {
	// 		$('#nama_barang').focus();
	// 	});
	// 	return false;
	// }
			
	$.get("data.php?inputBundleBarang=bundling_barang&nama_bundling="+nama_bundle+"&ids_barang_utama="+ids_barang_utama+"&diskon_bundling="+diskon_bundle+"&nama_barang_utama="+nama_barang_utama+"&ids_barang_bonus="+ids_barang_bonus+"&nama_barang_bonus="+nama_barang_bonus+"&jumlah_barang_utama="+jumlahUtama+"&jumlah_barang_bonus="+jumlahBonus,
	function(data){
		if(data==1){
			swal("Maaf ! ","Bundling barang sudah ada").then((value) => {
				$('#nama_bundle').focus();
				$('#nama_bundle').select();
			});
			return false;
		}else{
			table.ajax.url( ajaxData ).load();
			//$('#EditPost').modal('hide');
			swal(
			{
			title: 'Sukses!',
			text: 'Data berhasil ditambahkan',
			type: 'success',
			timer: 2000
			}
			);
		}
	});
	// $.get("data.php?pilihBarang="+ids_barang);
});


$( "#SaveEdit" ).click(function () {
	$("#EditPost").modal('hide');
	var nama_bundle = $('#nama_bundle').val();
	var nama_barang_utama = $("#nama_barang_utama").val();
	var ids_barang_utama = $('#ids_barang_utama').val();
	var diskon_bundle = $('#diskonBundle').val();
	var nama_barang_bonus = $("#nama_barang_bonus").val();
	var ids_barang_bonus = $('#ids_barang_bonus').val();
	var jumlahUtama = $('#jumlahUtama').val();
	var jumlahBonus = $('#jumlahBonus').val();
	var id = idBundle;

	if(ids_barang_utama==''){
		swal("","Belum ada barang utama yang dipilih!").then((value) => {
			$('#kode_barang').focus();
		});
		return false;
	}
	if(ids_barang_bonus==''){
		swal("","Belum ada barang bonus yang dipilih!").then((value) => {
			$('#kode_barang').focus();
		});
		return false;
	}
	// if(diskon_bundle==''){
	// 	swal("","Diskon Bundling masih kosong!").then((value) => {
	// 		$('#nama_barang').focus();
	// 	});
	// 	return false;
	// }
			
	$.get("data.php?editBundleBarang=bundling_barang&nama_bundling="+nama_bundle+"&ids_barang_utama="+ids_barang_utama+"&ids_barang_bonus="+ids_barang_bonus+"&diskon_bundling="+diskon_bundle+"&nama_barang_utama="+nama_barang_utama+"&nama_barang_bonus="+nama_barang_bonus+"&id="+id+"&jumlah_barang_utama="+jumlahUtama+"&jumlah_barang_bonus="+jumlahBonus,
	function(data){
		table.ajax.url( ajaxData ).load();
			swal({
				title: 'Sukses!',
				text: 'Data berhasil ditambahkan',
				type: 'success',
				timer: 2000
			});
	});
	// var splitId = idBarangPrev.split(", ");
	// var selectedId = ids_barang.split(", ");
	// var isSame = 0;
	
	// if(JSON.stringify(splitId) != JSON.stringify(selectedId)){
	// 	// $.get("data.php?pilihBarang="+ids_barang);
	// }
} );

// $( "#insertSatuan" ).click(function () {
// var satuan = $('#inputSatuan').val();
// $.get("data.php?inputSatuan=satuan&satuan="+satuan,
// function(data){
// 	tableSatuan.ajax.url( ajaxSatuan ).load();
// 	$('#inputSatuan').val('');
// 	$( "#selectSatuan" ).load( "data.php?satuanBarang=satuan" );
// }
// );

// });

// $( "#import" ).click(function () {
//  $('#modalImport').modal('show');
//  $('#fileExcel').val('');


// });
// $( "#insertKategori" ).click(function () {
// var satuan = $('#inputKategori').val();
// $.get("data.php?inputKategori=daftar_kategori&kategori="+satuan,
// function(data){
// 	tableKategori.ajax.url( ajaxKategori ).load();
// 	$('#inputKategori').val('');
// 	$( "#selectKategori" ).load( "data.php?kategoriBarang=kategori" );

// }
// );

			
// } );

$( "#refresh" ).click(function () {
	table.ajax.url( ajaxData ).load();
} );	
// $( "#stokLimit" ).click(function () {
// 	table.ajax.url( "data.php?tableBarangLimit=daftar_barang" ).load();
// } );	
	
// $( "#stokExpired" ).click(function () {
// 	table.ajax.url( "data.php?tableBarangExpired=daftar_barang" ).load();
// } );	
	
	
// $( "#showKategori" ).click(function () {
	
// 	$("#EditKategori").modal("show");
// } );	
	

	
} );
function brgID(){
	var imgID=document.getElementById("imgID");
	var b=document.getElementById("kode_barang");
	var a=""+Math.floor((Math.random()*10000)+1);
	var c=""+Math.floor((Math.random()*10000)+1);
	b.value=""+a+c
	imgID.value=""+a+c
	};
function showSatuan() {
		$('#EditSatuan').modal("show");
		tableSatuan.ajax.url( ajaxSatuan ).load();


}

function showKategori() {
		$('#EditKategori').modal("show");
		tableKategori.ajax.url( ajaxKategori ).load();


}
/*
function pilihSatuan(e) {
    document.getElementById("satuan").value = e.target.value;
		$('#selectSatuan').hide();

}
function pilihKategori(e) {
    document.getElementById("kategori_barang").value = e.target.value;
		$('#selectKategori').hide();

}
*/
String.prototype.reverse = function () {
        return this.split("").reverse().join("");
    }

    function reformatText(input) {        
        var x = input.value;
        x = x.replace(/,/g, ""); // Strip out all commas
        x = x.reverse();
        x = x.replace(/.../g, function (e) {
            return e + ",";
        }); // Insert new commas
        x = x.reverse();
        x = x.replace(/^,/, ""); // Remove leading comma
        input.value = x;
    }



</script>