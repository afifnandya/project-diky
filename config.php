<?php 
date_default_timezone_set("Asia/Jakarta");
// $CORE_URL			= "http://localhost/dpos";
$CORE_URL = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$CORE_URL .= "://".$_SERVER['HTTP_HOST'];
$CORE_URL .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
$SID				= "1002";
$SNAME				= "DPOS TOKO PRO";
$version			= file_get_contents("app/version.txt");
$database_engine	="mysql"; 					//Anda bisa mengganti dengan sqlite atau mysql


/*------------------------------------DATABSE MYSQL ------------------------------*/
if($database_engine=="mysql"){
	//MENGGUNAKAN MYSQL
	include 			"library/mysql.class.php";	//class mysql database
	$db 				= new Database();			// instance database
	$db->dbHost			="localhost";				// database host
	$db->dbUser			="root";					// database username
	$db->dbPass			=""; 						// dabatabse password
	$db->dbName			="db_kasir"; 				// database name
	$GLOBALS['db'] = $db;
	include 			"library/mysql.db.php";		// fungsi mysql

/*------------------------------------DATABSE SQLITE ------------------------------*/

}elseif($database_engine=="sqlite"){
	//MENGGUNAKAN SQLITE3
	$storagelocation 	= __DIR__ ."/"; 						//direktori root
	$db_file 			= $storagelocation."/\db\database.db";	//direktori menyimpan file database sqlite
	$db					= new SQLite3($db_file);				// instance database
	include 			"library/sqlite.db.php";				// fungsi sqlite
}
include "library/lib.php";
include "library/ox000fx.inc";

$APP_DIR= __DIR__."/app";
$ASSETS_DIR= __DIR__."/assets";
?>
