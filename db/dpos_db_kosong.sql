-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Agu 2018 pada 01.19
-- Versi server: 10.1.19-MariaDB
-- Versi PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kasir`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akses`
--

CREATE TABLE `akses` (
  `id` int(11) NOT NULL,
  `level` text,
  `master` int(11) NOT NULL,
  `kasir` int(11) DEFAULT NULL,
  `transaksi` int(11) DEFAULT NULL,
  `laporan` int(11) DEFAULT NULL,
  `pengguna` int(11) DEFAULT NULL,
  `pengaturan` int(11) DEFAULT NULL,
  `hak_akses` int(11) DEFAULT NULL,
  `barang` int(11) DEFAULT NULL,
  `barang_tambah` int(11) DEFAULT NULL,
  `barang_import` int(11) DEFAULT NULL,
  `barang_edit` int(11) DEFAULT NULL,
  `barang_hapus` int(11) DEFAULT NULL,
  `barcode` int(11) DEFAULT NULL,
  `transaksi_penjualan` int(11) DEFAULT NULL,
  `transaksi_pembelian` int(11) DEFAULT NULL,
  `transaksi_piutang` int(11) DEFAULT NULL,
  `transaksi_hutang` int(11) DEFAULT NULL,
  `transaksi_return_penjualan` int(11) DEFAULT NULL,
  `transaksi_return_pembelian` int(11) DEFAULT NULL,
  `laporan_kas` int(11) DEFAULT NULL,
  `laporan_laba_rugi` int(11) DEFAULT NULL,
  `laporan_grafik` int(11) DEFAULT NULL,
  `transaksi_return` int(11) DEFAULT NULL,
  `pelanggan` int(11) DEFAULT NULL,
  `pelanggan_tambah` int(11) DEFAULT NULL,
  `pelanggan_import` int(11) DEFAULT NULL,
  `pelanggan_edit` int(11) DEFAULT NULL,
  `pelanggan_hapus` int(11) DEFAULT NULL,
  `supplier` int(11) DEFAULT NULL,
  `supplier_tambah` int(11) DEFAULT NULL,
  `supplier_import` int(11) DEFAULT NULL,
  `supplier_edit` int(11) DEFAULT NULL,
  `supplier_hapus` int(11) DEFAULT NULL,
  `stok_opname` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `akses`
--

INSERT INTO `akses` (`id`, `level`, `master`, `kasir`, `transaksi`, `laporan`, `pengguna`, `pengaturan`, `hak_akses`, `barang`, `barang_tambah`, `barang_import`, `barang_edit`, `barang_hapus`, `barcode`, `transaksi_penjualan`, `transaksi_pembelian`, `transaksi_piutang`, `transaksi_hutang`, `transaksi_return_penjualan`, `transaksi_return_pembelian`, `laporan_kas`, `laporan_laba_rugi`, `laporan_grafik`, `transaksi_return`, `pelanggan`, `pelanggan_tambah`, `pelanggan_import`, `pelanggan_edit`, `pelanggan_hapus`, `supplier`, `supplier_tambah`, `supplier_import`, `supplier_edit`, `supplier_hapus`, `stok_opname`) VALUES
(1, 'master', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 'marketing', 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 'kasir', 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(4, 'operator', 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `daftar_barang`
--

CREATE TABLE `daftar_barang` (
  `id_barang` int(11) NOT NULL,
  `kode_barang` text,
  `nama_barang` text,
  `kategori_barang` text,
  `satuan` text,
  `harga_beli` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `diskon` text,
  `terjual` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `lokasi` text,
  `ukuran` text,
  `warna` text,
  `merek` text,
  `expired` text,
  `stok_minimal` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `expiredDate` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dashboard`
--

CREATE TABLE `dashboard` (
  `total_barang` int(11) DEFAULT NULL,
  `total_pelanggan` int(11) DEFAULT NULL,
  `omset` int(11) DEFAULT NULL,
  `laba` int(11) DEFAULT NULL,
  `grafik` int(11) DEFAULT NULL,
  `barang_terlaris` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `barang_expired` int(11) DEFAULT NULL,
  `barang_limit` int(11) DEFAULT NULL,
  `piutang_tempo` int(11) DEFAULT NULL,
  `hutang_rempo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `dashboard`
--

INSERT INTO `dashboard` (`total_barang`, `total_pelanggan`, `omset`, `laba`, `grafik`, `barang_terlaris`, `id`, `barang_expired`, `barang_limit`, `piutang_tempo`, `hutang_rempo`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ekspedisi`
--

CREATE TABLE `ekspedisi` (
  `id` int(11) NOT NULL,
  `ekspedisi` text,
  `website` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `ekspedisi`
--

INSERT INTO `ekspedisi` (`id`, `ekspedisi`, `website`) VALUES
(1, 'JNE', 'http://jne.co.id'),
(2, 'JNT', ''),
(3, 'TIKI', ''),
(4, 'POS', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `faktur`
--

CREATE TABLE `faktur` (
  `id` int(11) NOT NULL,
  `faktur` text,
  `pelanggan_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` int(11) DEFAULT NULL,
  `bulan` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `date` text,
  `ongkir` int(11) DEFAULT NULL,
  `ekspedisi` text,
  `total` int(11) DEFAULT NULL,
  `pemasukan` text,
  `pengeluaran` text,
  `mode` text,
  `keterangan` text,
  `voucher` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `grand_total` int(11) DEFAULT NULL,
  `dibayar` int(11) DEFAULT NULL,
  `kembali` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` text,
  `hutang` int(11) DEFAULT NULL,
  `hutang_dibayar` int(11) DEFAULT NULL,
  `hutang_sisa` int(11) DEFAULT NULL,
  `tempo` text,
  `disc` int(11) DEFAULT NULL,
  `pjk` int(11) DEFAULT NULL,
  `pajak` int(11) DEFAULT NULL,
  `total_hpp` int(11) DEFAULT NULL,
  `laba_rugi` int(11) DEFAULT NULL,
  `terjual` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kasir_pembelian`
--

CREATE TABLE `kasir_pembelian` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `faktur` text,
  `date` text,
  `nama_barang` text,
  `kode_barang` text,
  `status` int(11) DEFAULT NULL,
  `session` int(11) DEFAULT NULL,
  `hpp` int(11) DEFAULT NULL,
  `total_hpp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kasir_penjualan`
--

CREATE TABLE `kasir_penjualan` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `faktur` text,
  `date` text,
  `nama_barang` text,
  `kode_barang` text,
  `status` int(11) DEFAULT NULL,
  `session` int(11) DEFAULT NULL,
  `hpp` int(11) DEFAULT NULL,
  `total_hpp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_barang`
--

CREATE TABLE `kategori_barang` (
  `id` int(11) NOT NULL,
  `kategori` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kategori_barang`
--

INSERT INTO `kategori_barang` (`id`, `kategori`) VALUES
(53, 'OMSET'),
(56, 'baju anak'),
(58, 'MINUMAN'),
(60, 'BATERAI'),
(61, ''),
(62, 'kosmetik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `modul`
--

CREATE TABLE `modul` (
  `id` int(11) NOT NULL,
  `ongkir` int(11) DEFAULT NULL,
  `pajak` int(11) DEFAULT NULL,
  `voucher` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `modul`
--

INSERT INTO `modul` (`id`, `ongkir`, `pajak`, `voucher`) VALUES
(1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id` int(11) NOT NULL,
  `nama_pelanggan` text,
  `alamat` text,
  `kota` text,
  `no_hp` text,
  `email` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `admin` text,
  `password` text,
  `nama_toko` text,
  `keterangan` text,
  `alamat` text,
  `no_hp` text,
  `email` text,
  `website` text,
  `pin_bb` text,
  `facebook` text,
  `backup` datetime DEFAULT NULL,
  `serial` text,
  `aktivasi` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `admin`, `password`, `nama_toko`, `keterangan`, `alamat`, `no_hp`, `email`, `website`, `pin_bb`, `facebook`, `backup`, `serial`, `aktivasi`) VALUES
(1, '', '', 'DJAVASOFT.COM', '', 'Jl. P. Sudirman gang 7 no 56 A Tulungagung', '0888998890', 'toko@mail.com', 'http://djavasoft.com', '', '', '0000-00-00 00:00:00', '9604-CBJS', '1AE-553-410-A87');

-- --------------------------------------------------------

--
-- Struktur dari tabel `return_barang`
--

CREATE TABLE `return_barang` (
  `id` int(11) NOT NULL,
  `faktur` text,
  `pelanggan_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `tanggal` int(11) DEFAULT NULL,
  `bulan` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `date` text,
  `total` int(11) DEFAULT NULL,
  `return_penjualan` text,
  `return_pembelian` text,
  `mode` text,
  `keterangan` text,
  `dibayar` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` text,
  `return_total` int(11) DEFAULT NULL,
  `return_dibayar` int(11) DEFAULT NULL,
  `return_sisa` int(11) DEFAULT NULL,
  `faktur_pembelian` text,
  `faktur_penjualan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `return_pembelian`
--

CREATE TABLE `return_pembelian` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `faktur` text,
  `date` text,
  `nama_barang` text,
  `kode_barang` text,
  `status` int(11) DEFAULT NULL,
  `session` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `return_penjualan`
--

CREATE TABLE `return_penjualan` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `faktur` text,
  `date` text,
  `nama_barang` text,
  `kode_barang` text,
  `status` int(11) DEFAULT NULL,
  `session` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuan`
--

CREATE TABLE `satuan` (
  `id` int(11) NOT NULL,
  `satuan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `satuan`
--

INSERT INTO `satuan` (`id`, `satuan`) VALUES
(39, 'BH'),
(40, 'PAK'),
(42, 'LBR'),
(43, 'DOS'),
(44, 'PAD'),
(45, 'BTL'),
(46, 'BOX'),
(47, 'PAP'),
(48, 'BJ'),
(49, 'ISI'),
(50, 'BKS'),
(51, 'PCS'),
(53, 'KLG'),
(55, 'SET'),
(56, 'KRG'),
(58, 'KTL'),
(59, 'SCH');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok_opname`
--

CREATE TABLE `stok_opname` (
  `id` int(11) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `kode_barang` text,
  `nama_barang` text,
  `harga_beli` int(11) DEFAULT NULL,
  `stok_komputer` int(11) DEFAULT NULL,
  `stok_nyata` int(11) DEFAULT NULL,
  `selisih` int(11) DEFAULT NULL,
  `total_selisih` int(11) DEFAULT NULL,
  `date` text,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `nama_supplier` text,
  `alamat` text,
  `kota` text,
  `no_hp` text,
  `email` text,
  `website` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` text,
  `password` text,
  `level` text,
  `nama` text,
  `alamat` text,
  `no_hp` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `nama`, `alamat`, `no_hp`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'master', 'Administrator', 'Local', '0877889809'),
(12, 'kasir', 'c7911af3adbd12a035b289556d96470a', 'kasir', 'Kasir', '', ''),
(13, 'marketing', 'c769c2bd15500dd906102d9be97fdceb', 'marketing', 'Marketing', '', ''),
(14, 'operator', '4b583376b2767b923c3e1da60d10de59', 'operator', 'operator', '', '');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `daftar_barang`
--
ALTER TABLE `daftar_barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indeks untuk tabel `dashboard`
--
ALTER TABLE `dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ekspedisi`
--
ALTER TABLE `ekspedisi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `faktur`
--
ALTER TABLE `faktur`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kasir_pembelian`
--
ALTER TABLE `kasir_pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kasir_penjualan`
--
ALTER TABLE `kasir_penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori_barang`
--
ALTER TABLE `kategori_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `modul`
--
ALTER TABLE `modul`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `return_barang`
--
ALTER TABLE `return_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `return_pembelian`
--
ALTER TABLE `return_pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `return_penjualan`
--
ALTER TABLE `return_penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `stok_opname`
--
ALTER TABLE `stok_opname`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `akses`
--
ALTER TABLE `akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `daftar_barang`
--
ALTER TABLE `daftar_barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `dashboard`
--
ALTER TABLE `dashboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `ekspedisi`
--
ALTER TABLE `ekspedisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `faktur`
--
ALTER TABLE `faktur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kasir_pembelian`
--
ALTER TABLE `kasir_pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kasir_penjualan`
--
ALTER TABLE `kasir_penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kategori_barang`
--
ALTER TABLE `kategori_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT untuk tabel `modul`
--
ALTER TABLE `modul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `return_barang`
--
ALTER TABLE `return_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `return_pembelian`
--
ALTER TABLE `return_pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `return_penjualan`
--
ALTER TABLE `return_penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT untuk tabel `stok_opname`
--
ALTER TABLE `stok_opname`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
