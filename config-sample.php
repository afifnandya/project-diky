<?php 
/*
Aplikasi ini dibuat oleh djavasoft.com
Kontak : contact.djavasoft@gmail.com

*/

date_default_timezone_set('Asia/Jakarta');
$SID				= '1002';
$SNAME				= 'DPOS TOKO PRO';
$version			= '2.6.1';
$CORE_URL			= 'http://localhost/dpos';
$database_engine	='mysql'; 					//Anda bisa mengganti dengan sqlite atau mysql


/*------------------------------------DATABSE MYSQL ------------------------------*/
if($database_engine=='mysql'){
	//MENGGUNAKAN MYSQL
	include 			'library/mysql.class.php';	//class mysql database
	$db 				= new Database();			// instance database
	$db->dbHost			='localhost';				// database host
	$db->dbUser			='root';					// database username
	$db->dbPass			=''; 						// dabatabse password
	$db->dbName			='db'; 				// database name
	include 			'library/mysql.db.php';		// fungsi mysql

/*------------------------------------DATABSE SQLITE ------------------------------*/

}elseif($database_engine=='sqlite'){
	//MENGGUNAKAN SQLITE3
	$storagelocation 	= __DIR__ .'/'; 						//direktori root
	$db_file 			= $storagelocation."db/database.db";	//direktori menyimpan file database sqlite
	$db					= new SQLite3($db_file);				// instance database
	include 			'library/sqlite.db.php';				// fungsi sqlite
}
include 'library/lib.php';
include 'library/ox000fx.inc';

$APP_DIR= __DIR__.'/app';
$ASSETS_DIR= __DIR__.'/assets';


?>

