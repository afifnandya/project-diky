<?php 
ob_start();
session_start() ;
error_reporting(1);
if(!file_exists('config.php')){
	header('location:setup.php');
} 

include 'config.php';


if(!get_session()) {
header("location:login.php");
}
if (isset($_GET['page']) AND $_GET['page'] == 'logout')
{
user_logout();
header("location:login.php");
}
include $APP_DIR.'/index.php';

?>

