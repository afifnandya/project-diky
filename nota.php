	<?php 
	include'config.php';
	function xdate($date){
		$date=explode('-',$date);	
		$date=$date[2].'-'.$date[1].'-'.$date[0];	
		return $date;
	}

	$items=doTableArray("kasir_penjualan",array("kode_barang","nama_barang","harga","qty","total","faktur"),"WHERE status=2 AND faktur='".$_GET['faktur']."'");
	$faktur=doTableArray("faktur",array("date","pelanggan_id","total","pemasukan","diskon","voucher","dibayar","kembali","user_id","disc","pajak","pjk","pajakDebit","nominalPjk"),"where faktur='".$_GET['faktur']."'");
	$date= $faktur[0][0];
	$pelanggan_id= $faktur[0][1];
	$total= $faktur[0][2];
	$pemasukan= $faktur[0][3];
	$diskon= $faktur[0][4];
	$voucher= $faktur[0][5];
	$dibayar= $faktur[0][6];
	$kembali= $faktur[0][7];
	$userName= userName($faktur[0][8]);
	$disc= $faktur[0][9];
	$pajak= $faktur[0][10];
	$pjk= $faktur[0][11];
	$pajakDebit = $faktur[0][12];
	$pjkDebit = $faktur[0][13];

	/*if(intval($pelanggan_id) !='' || intval($pelanggan_id)!=0){
	$supplier=doTableArray("pelanggan",array("nama_pelanggan"),"where id='".intval($pelanggan_id)."'");
	$nama_pelanggan=$supplier[0][0];
	}else{
	$nama_pelanggan='-';
	}
	*/
	?>
	<html moznomarginboxes="" mozdisallowselectionprint=""><head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>	<?php echo getPengaturan('nama_toko');?> - Cetak Nota        </title>

	<?php 
	if($_GET['ukuran']=='58'){
	?>
		<style>
		html,body{
			font-size: 6px !important;
			margin: 0px !important;
			font-family: 'Courier New';
		}
		@page { 
			size: 58mm 9cm;
			margin: 1mm 1mm 1mm 1mm; 
		}
		</style>
	<?php } elseif($_GET['ukuran']=='80'){ ?>
		<style>
		html,body{
			font-size: 8px !important;
			margin: 0px !important;
			font-family: 'Courier New';
		}
		@page { 
			size: 88mm 9cm;
			margin: 1mm 1mm 1mm 1mm; 
		}
		</style>
	<?php } elseif($_GET['ukuran']== '24-14-cm'){?>
		<style>
		html,body{
			font-size: 14px !important;
			margin: 0px !important;
			font-family: 'Courier New';
		}
		@page { 
			size: 24cm 14cm;
			margin: 7cm !important; 
		}
		</style>
	<?php } ?>
	</head>
	<body class="receipt" onload="window.print();" cz-shortcut-listen="true" >
	<section class="sheet padding-10mm">
<?php 
if($_GET['ukuran']=='58'){?>
<div style="display: flex;align-items: center;justify-content:start">
	<img src="./assets/logo.png" alt="" style="width:25px;margin-right: 2px">
	<b style="font-size: 10px"><?php echo getPengaturan('nama_toko');?></b>
</div>
<br><?php echo getPengaturan('alamat');?> <br>Telp. <?php echo getPengaturan('no_hp');?><br>---------------------------------<br>
DATE	: <?php echo xDate($date);?> <?php echo date("H:i:s");?><br>FAKTUR	: <?php echo $_GET['faktur'];?><br>KASIR	: <?php echo $userName;?>
<br>---------------------------------<br> Harga	Qty	Total<br>---------------------------------<br>
<?php
$i=1;
foreach($items as $row){
echo ''.$row[1].'<br>';
echo ' '. currency($row[2]).'';
echo ' x ';
echo ''.$row[3].'	';
echo 'Rp.'. currency($row[4]).'<br>';
$i++; 	  
}   
?>
---------------------------------
<?php if(intval($diskon)!=0 OR intval($diskon)!=''){?>
Harga	:	Rp.<?php echo currency($total);?><br>
Diskon	:	Rp.<?php echo currency($diskon);?>(<?php echo $disc;?>%)<?php } ?><?php if(intval($voucher)!=0 OR intval($voucher)!=''){?><br>
Voucher	:	Rp.<?php echo currency($voucher);?><?php } ?><?php if(intval($pjk)!=0 OR intval($pjk)!=''){?><br>
Pajak	:	Rp.<?php echo currency($pajak);?>(<?php echo $pjk;?>%)<?php } ?><br> <?php if(intval($pajakDebit)!=0 OR intval($pajakDebit)!=''){?><br>
EDC(<?php echo ($pjkDebit == 0.3) ? "Sama Bank" : "Beda Bank"; ?>)     :       Rp.<?php echo currency($pajakDebit);?>(<?php echo $pjkDebit;?>%)<?php } ?><br>
Total	:	Rp.<?php echo currency($pemasukan);?><br>Bayar	:	Rp.<?php echo currency($dibayar);?>
<br>	-------------------------<br>Kembali	:	Rp.<?php echo currency($kembali);?><br>=================================<br>
	TERIMA KASIH
    ATAS KUNJUNGAN ANDA
<BR>

<?php } elseif($_GET['ukuran']=='80'){ 
/////////////////////////////////////// UKURAN 80MM ////////////////////////////////////////////////////////
?>
<div style="display: flex;align-items: center;justify-content:start">
	<img src="./assets/logo.png" alt="" style="width:25px;margin-right: 2px">
	<b style="font-size: 10px"><?php echo getPengaturan('nama_toko');?></b>
</div>
<br><?php echo getPengaturan('alamat');?> <br>Telp/HP <?php echo getPengaturan('no_hp');?><br>--------------------------------------<br>
DATE	: <?php echo xDate($date);?> <?php echo date("H:i:s");?><br>FAKTUR	: <?php echo $_GET['faktur'];?><br>KASIR	: <?php echo $userName;?>
<br>--------------------------------------<br> Harga		Qty	Total<br>--------------------------------------<br>
<?php
$i=1;
foreach($items as $row){
echo ''.$row[1].'<br>';
echo ' '. currency($row[2]).'';
echo '		x ';
echo ''.$row[3].'	';
echo 'Rp.'. currency($row[4]).'<br>';
$i++; 	  
}   
?>
--------------------------------------
 <?php 
 if(intval($diskon)!=0 OR intval($diskon)!=''){?>
 Harga		:	Rp.<?php echo currency($total);?><br> 
 Diskon		:		Rp.<?php echo currency($diskon);?>(<?php echo $disc;?>%)<?php }?>
 <?php if(intval($voucher)!=0 OR intval($voucher)!=''){?><br>
Voucher		:		Rp.<?php echo currency($voucher);?><?php } ?>
<?php if(intval($pjk)!=0 OR intval($pjk)!=''){?><br> 
Pajak		:	Rp.<?php echo currency($pajak);?>(<?php echo $pjk;?>%)<?php } ?><br>
<?php if(intval($pajakDebit)!=0 OR intval($pajakDebit)!=''){?><br>
EDC(<?php echo ($pjkDebit == 0.3) ? "Sama Bank" : "Beda Bank"; ?>)     :       Rp.<?php echo currency($pajakDebit);?>(<?php echo $pjkDebit;?>%)<?php } ?><br>
Total		:	Rp.<?php echo currency($pemasukan);?><br> 
Bayar		:	Rp.<?php echo currency($dibayar);?><br>		----------------------<br> Kembali	:	Rp.<?php echo currency($kembali);?><br>======================================<br>
	   TERIMA KASIH
	ATAS KUNJUNGAN ANDA
<BR>

<?php } elseif($_GET['ukuran']=='24-14-cm'){ 
/////////////////////////////////////// UKURAN 24 cm ////////////////////////////////////////////////////////
?>
<div style="display: flex;align-items: center;justify-content:start">
	<img src="./assets/logo.png" alt="" style="width:100px;margin-right: 2px">
	<b style="font-size: 25px"><?php echo getPengaturan('nama_toko');?></b>
</div>
<p style="border-bottom:1px dashed #333333;width:100%;">
	<?php echo getPengaturan('alamat');?> <br>Telp/HP <?php echo getPengaturan('no_hp');?>
</p>
<p style="border-bottom:1px dashed #333333;width:100%;">
DATE	: <?php echo xDate($date);?> <?php echo date("H:i:s");?><br>FAKTUR	: <?php echo $_GET['faktur'];?><br>KASIR	: <?php echo $userName;?>
</p>
<table style="table-layout: auto; width: 100%;">
	<thead>
		<th style="border-bottom:1px dashed #333333;width:100px;">Harga</th>
		<th style="border-bottom:1px dashed #333333;width:100px;">Qty</th>
		<th style="border-bottom:1px dashed #333333;width:100px;">Total</th>
	</thead>
	<tbody>
	<?php
		$i=1;
		foreach($items as $row){
			// echo ''.$row[1];
			// echo ' '. currency($row[2]).'';
			// echo '		x ';
			// echo ''.$row[3].'	';
			// echo 'Rp.'. currency($row[4]).'<br>';
			 	     
	?>
		<tr>
			<td><?php echo $row[1];?></td>
			<td><?php echo currency($row[2]).'x'.$row[3];?></td>
			<td><?php echo 'Rp.'. currency($row[4]);?></td>
		</tr>
	<?php 
		$i++;
		} 
	?>
	</tbody>
</table>
<p style="border-bottom:2px dashed #333333;width:100%;">
 <?php 
 if(intval($diskon)!=0 OR intval($diskon)!=''){?>
 Harga		:	Rp.<?php echo currency($total);?><br> 
 Diskon		:		Rp.<?php echo currency($diskon);?>(<?php echo $disc;?>%)<?php }?>
 <?php if(intval($voucher)!=0 OR intval($voucher)!=''){?><br>
Voucher		:		Rp.<?php echo currency($voucher);?><?php } ?>
<?php if(intval($pjk)!=0 OR intval($pjk)!=''){?><br> 
Pajak		:	Rp.<?php echo currency($pajak);?>(<?php echo $pjk;?>%)<?php } ?><br>
<?php if(intval($pajakDebit)!=0 OR intval($pajakDebit)!=''){?><br>
EDC(<?php echo ($pjkDebit == 0.3) ? "Sama Bank" : "Beda Bank"; ?>)     :       Rp.<?php echo currency($pajakDebit);?>(<?php echo $pjkDebit;?>%)<?php } ?><br>
Total		:	Rp.<?php echo currency($pemasukan);?><br> 
Bayar		:	Rp.<?php echo currency($dibayar);?>
</p>		 
<p style="border-bottom:1px dashed #333333;width:100%;">
Kembali	:	Rp.<?php echo currency($kembali);?>
</p>
<p style="border-bottom:1px dashed #333333;width:100%; text-align:center;">
	   TERIMA KASIH
	ATAS KUNJUNGAN ANDA
</p>

<?php } ?>

</section>
	
	</body></html>
