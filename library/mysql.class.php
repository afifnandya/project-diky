<?php
class Database {
	// properti
	public $dbHost;
	public $dbUser;
	public $dbPass;
    public $dbName;
    

	// method koneksi mysql
	function connectMySQL() {
		// mysql_connect($this->dbHost, $this->dbUser, $this->dbPass);
        // mysql_select_db($this->dbName) or die ("<center>Database Tidak Ditemukan di Server.<br/>Silakana melakaukan instalasi <a href='install.php'>disini</a></center> "); 		
        $conn = mysqli_connect($this->dbHost,$this->dbUser,$this->dbPass,$this->dbName);
		// $conn = mysql_connect($this->dbHost,$this->dbUser,$this->dbPass);
		if(!$conn) die("Failed to connect to database!");
		// $status = mysql_select_db($this->dbName, $conn);
		// if(!$status) die("Failed to select database!");
		return $conn;	
	}
	function connect() {
        return $this ->connectMySQL();
	}
	
		function query($query)
	{
	global $dbUser, $dbPass, $dbHost, $dbname; 	
		$conn = $this ->connectMySQL() or die("cannot connect to db");
		// mysql_select_db($this->dbName)or die("cannot select DB");
		$result = mysqli_query($conn,$query); 
		return $result; 
	}

	    private function table_exists($table)
    {
        $conn = $this ->connectMySQL() or die("cannot connect to db");
        $tablesInDb = @mysqli_query($conn,'SHOW TABLES FROM '.$this->db_name.' LIKE "'.$table.'"');
        if($tablesInDb)
        {
            if(mysqli_num_rows($tablesInDb)==1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
	
	public function select($table, $rows = '*', $where = null, $order = null){	
        $conn = $this ->connectMySQL();
        if(!$conn) die("Failed to connect to database!");
		$sql = 'SELECT '.$rows.' FROM '.$table;
        if($where != null)
            $sql .= ' WHERE '.$where;
        if($order != null)
            $sql .= ' ORDER BY '.$order;
			
		$result=mysqli_query($conn,$sql);
			
		return $result;
	}

    public function insert($table,$rows = null,$values)
    {
        $conn = $this ->connectMySQL() or die("cannot connect to db");
        if($this->table_exists($table))
        {
            $insert = 'INSERT INTO '.$table;
            if($rows != null)
            {
                $insert .= ' ('.$rows.')';
            }

            for($i = 0; $i < count($values); $i++)
            {
                if(is_string($values[$i]))
                    $values[$i] = '"'.$values[$i].'"';
            }
            $values = implode(',',$values);
            $insert .= ' VALUES ('.$values.')';

            $ins = @mysqli_query($conn,$insert);

            if($ins)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public function delete($table,$where = null)
    {
        $conn = $this ->connectMySQL() or die("cannot connect to db");
        if($this->table_exists($table))
        {
            if($where == null)
            {
                $delete = 'DELETE '.$table;
            }
            else
            {
                $delete = 'DELETE FROM '.$table.' WHERE '.$where;
            }
            $del = @mysqli_query($conn,$delete);

            if($del)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }	
    public function update($table,$rows,$where)
    {
        $conn = $this ->connectMySQL() or die("cannot connect to db");
        if($this->table_exists($table))
        {
            // Parse mana nilai-nilai
            // Bahkan nilai-nilai (termasuk 0) mengandung baris mana
            // Nilai ganjil mengandung klausul untuk baris
			
            $update = 'UPDATE '.$table.' SET ';
            $keys = array_keys($rows);
            for($i = 0; $i < count($rows); $i++)
            {
                if(is_string($rows[$keys[$i]]))
                {
                    $update .= $keys[$i].'="'.$rows[$keys[$i]].'"';
                }
                else
                {
                    $update .= $keys[$i].'='.$rows[$keys[$i]];
                }

                // Parse to add commas
                if($i != count($rows)-1)
                {
                    $update .= ',';
                }
            }
            $update .= ' WHERE '.$where;
            $query = @mysqli_query($conn,$update);
            if($query)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
?>